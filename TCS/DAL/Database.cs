﻿namespace TCS.DAL
{
    /// <summary>
    /// An inherited class to hold settings and provide shortcuts
    /// </summary>
    public class Database : TCSEntities
    {
        public static Database Context
        {
            get
            {
                var db = new Database();
                db.Configuration.LazyLoadingEnabled = false;
                return db;
            }
        }

        public static Database ContextReadOnly
        {
            get
            {
                var db = new Database();
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.AutoDetectChangesEnabled = false;
                return db;
            }
        }
    }
}