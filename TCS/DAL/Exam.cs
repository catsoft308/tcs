//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TCS.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Exam
    {
        public Exam()
        {
            this.Appointments = new HashSet<Appointment>();
        }
    
        public int ID { get; set; }
        public Nullable<int> classID { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public Nullable<int> Approved { get; set; }
        public Nullable<int> TERM_ID { get; set; }
        public Nullable<int> DURATION { get; set; }
        public string NAME { get; set; }
    
        public virtual ICollection<Appointment> Appointments { get; set; }
        public virtual Term Term { get; set; }
        public virtual User CreatedByUser { get; set; }
        public virtual Class Class { get; set; }
    }
}
