//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TCS.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class open_dates
    {
        public int TERM_ID { get; set; }
        public int SBU_FLG { get; set; }
        public Nullable<System.DateTime> START_DATE { get; set; }
        public Nullable<System.DateTime> END_DATE { get; set; }
    
        public virtual Term Term { get; set; }
    }
}
