﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TCS.App_Data;
using TCS.DAL;
using TCS.Services;

namespace TCS.Controllers
{
    public class AdminController : Controller
    {
        //------ GETS ------
        public ActionResult Home()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult Exams()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult Utilization()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult ImportData()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult EditCenterInfo()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult Reports()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult ScheduleExamForStudent()
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult MakeAppointment(int examID, string studentID)
        {
            if (UserService.UserHasRole(UserService.ADMIN_ROLE))
            {
                System.Web.HttpContext.Current.Session[Constants.SessionStrings.CURRENT_EXAM] = examID;
                System.Web.HttpContext.Current.Session[Constants.SessionStrings.STUDENT_ID] = studentID;
                return View();
            }
            return RedirectToAction("Login", "Home");
        }


        //------ POSTS ------
        #region Exams
        public JsonResult GetExams(int termID)
        {
            using (var db = new TCSEntities())
            {
                // Get exams for this term
                var exams = (from e in db.Exam
                             join c in db.Class on e.classID equals c.ID
                             where e.TERM_ID == termID
                             select new { e, c }).AsEnumerable().Select(item => new
                             {
                                 id = item.e.ID,
                                 termID = item.e.TERM_ID,
                                 courseID = item.c.Subject + " " + item.c.CatalogID,
                                 name = item.e.NAME,
                                 createdBy = item.e.CreatedBy,
                                 startTimeISOString = item.e.StartTime.ToString(),
                                 endTimeISOString = item.e.EndTime.ToString(),
                                 startTime = item.e.StartTime.Value.ToShortDateString() + " at " + item.e.StartTime.Value.ToShortTimeString(),
                                 endTime = item.e.EndTime.Value.ToShortDateString() + " at " + item.e.EndTime.Value.ToShortTimeString(),
                                 duration = item.e.DURATION,
                                 approved = item.e.Approved == 1
                             });
                // Differentiate between approved and not
                return Json(new {
                    approvedExams = exams.Where(e => e.approved).ToList(),
                    pendingExams = exams.Where(e => !e.approved).ToList()
                });
            }
        }

        public JsonResult GetExamInfo(int examID)
        {
            using (var db = new TCSEntities())
            {
                // Get all student appointments for the given exam
                var appointments = (from a in db.Appointment
                                    where a.ExamID == examID
                                    select a).AsEnumerable().Select(a => new
                                    {
                                        id = a.ID,
                                        seatNumber = a.AssignedSeat,
                                        studentID = a.OwnerID,
                                        time = a.APPT_TIME.Value,
                                        timeString = a.APPT_TIME.Value.ToShortDateString() + " at " + a.APPT_TIME.Value.ToShortTimeString(),
                                        attended = a.CheckedIn == 1
                                        //TODO: include availability incase you want to change the seat/time for an appointment
                                    });
                return Json(appointments.ToList());
            }
        }

        public string EditAppointment(int appointmentID, int seatNumber, DateTime date, DateTime time)
        {
            using (var db = new TCSEntities())
            {
                var appointment = (from a in db.Appointment
                                   where a.ID == appointmentID
                                   select a).FirstOrDefault();
                if (appointment == null)
                    return "Invalid appoinment.";
                var origSeat = appointment.AssignedSeat;
                var origDate = appointment.APPT_TIME.Value.ToShortDateString();
                var origTime = appointment.APPT_TIME.Value.TimeOfDay.ToString();
                appointment.AssignedSeat = seatNumber;
                appointment.APPT_TIME = date.Date + time.TimeOfDay;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }

                var email = (from u in db.User
                             where u.ID == appointment.OwnerID
                             select u.Email).First();

                var examName = (from e in db.Exam
                                where e.ID == appointment.ExamID
                                select e.NAME).First();

                EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, email,
                    "Frey Hall Testing Center Appointment Edited",
                    "<p>Your Appointment for " + examName + " has been edited:</p>"
                    + "<p>Time was changed from " + origDate + " at " + origTime +
                    " to " + date.ToShortDateString() + " at " + time.TimeOfDay.ToString() +
                    " and seat changed from " + origSeat + " to " + seatNumber + ".</p>");
                return "Appointment time successfully changed from " + origDate + " at " + origTime +
                    " to " + date.ToShortDateString() + " at " + time.TimeOfDay.ToString() + 
                    " and seat changed from " + origSeat + " to " + seatNumber + ".";
            }
        }

        public string CheckInStudent(int appointmentID)
        {
            using (var db = new TCSEntities())
            {
                var appointment = (from a in db.Appointment
                                   where a.ID == appointmentID
                                   select a).FirstOrDefault();
                if (appointment == null)
                    return "Invalid appointment.";
                appointment.CheckedIn = 1;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return appointment.User.ID + " successfully checked in for " + appointment.Exam.NAME + ".";
            }
        }

        public string CancelAppointment(int appointmentID)
        {
            using (var db = new TCSEntities())
            {
                var info = (from a in db.Appointment
                            join u in db.User on a.OwnerID equals u.ID
                            join e in db.Exam on a.ExamID equals e.ID
                            where a.ID == appointmentID
                            select new { email = u.Email, examName = e.NAME }).First();

                var approved = AppointmentService.cancelAppointment(appointmentID);

                if (!approved)
                    return "Couldn't cancel appointment.";

                EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, info.email,
                    "Frey Hall Testing Center Appointment Edited",
                    "<p>Your Appointment for " + info.examName + " has been cancelled.</p>");
                return "Appointment successfully cancelled."; 
            }
        }

        public string CancelExam(int examID)
        {
            using (var db = new TCSEntities())
            {
                var exam = (from e in db.Exam
                            where e.ID == examID
                            select e).FirstOrDefault();
                if (exam == null)
                    return "Invalid exam. Could not cancel.";
                exam.Approved = 0;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, exam.CreatedByUser.Email,
                    "Frey Hall Exam Cancelled",
                    "<p>Your exam " + exam.NAME + " has been cancelled.</p>");
                return exam.NAME + " successfully canceled.";
            }
        }

        public string ApproveRequest(int examID)
        {
            using (var db = new TCSEntities())
            {
                var exam = (from e in db.Exam
                            where e.ID == examID
                            select e).FirstOrDefault();
                if (exam == null)
                    return "Invalid exam. Could not approve request.";
                exam.Approved = isSchedulable(exam.ID);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, exam.CreatedByUser.Email,
                    "Frey Hall Exam Request Approved",
                    "<p>Your request for " + exam.NAME + " has been approved.</p>");
                return "Request for " + exam.NAME + " successfully approved.";
            }
        }

        public int isSchedulable(int ID)
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var exam = (from e in db.Exam
                            where e.ID == ID
                            select e).FirstOrDefault();

                var currentTerm = from t in db.Term
                                  where t.ID == exam.TERM_ID
                                  select new { t.WKDY_OPEN, t.WKDY_CLOSE, t.WKND_OPEN, t.WKND_CLOSE };
                var termTimes = currentTerm.AsEnumerable().Select(tt => new
                {
                    wkdyOpen = tt.WKDY_OPEN,
                    wkdyClose = tt.WKDY_CLOSE,
                    wkndOpen = tt.WKND_OPEN,
                    wkndClose = tt.WKND_CLOSE
                }).First();

                int studentCount = ExamService.StudentsWhoNeedToTake(exam).Count;
                int availAppts = 0;
                DateTime next = ((DateTime)exam.StartTime);

                while (next < ((DateTime)exam.EndTime))
                {
                    TimeSpan open = (next.DayOfWeek == DayOfWeek.Saturday || next.DayOfWeek == DayOfWeek.Sunday) ? ((TimeSpan)termTimes.wkndOpen) : ((TimeSpan)termTimes.wkdyOpen);
                    TimeSpan close = (next.DayOfWeek == DayOfWeek.Saturday || next.DayOfWeek == DayOfWeek.Sunday) ? ((TimeSpan)termTimes.wkndClose) : ((TimeSpan)termTimes.wkdyClose);

                    if (next.TimeOfDay >= open && next.TimeOfDay < close.Subtract(TimeSpan.FromMinutes((int)exam.DURATION)))
                    {
                        availAppts += AppointmentService.getNumApptsAvailAtTime(next, db);
                        if (availAppts >= studentCount)
                            return 1;
                    }
                    next = next.AddMinutes(30);
                }

            }
            return 0;
        }

        public string DenyRequest(int examID)
        {
            using (var db = new TCSEntities())
            {
                var exam = (from e in db.Exam
                            where e.ID == examID
                            select e).FirstOrDefault();
                if (exam == null)
                    return "Invalid exam. Could not deny request.";
                db.Exam.Remove(exam);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, exam.CreatedByUser.Email,
                    "Frey Hall Exam Request Denied",
                    "<p>Your request for " + exam.NAME + " was denied.</p>");
                return "Request for " + exam.NAME + " successfully denied.";
            }
        }
        #endregion

        #region EditCenterInfo
        public JsonResult GetCurrentCenterInfo(int termID)
        {
            using (var db = new TCSEntities())
            {
                var termInfo = (from s in db.SystemValues
                                join t in db.Term on termID equals t.ID
                                select new { s,t }).AsEnumerable().Select(info => new
                                {
                                    numSeats = info.s.NBR_SEATS,
                                    numSetAsideSeats = (int)info.s.SET_ASIDE_SEATS,
                                    centerHours = GetHours(termID),
                                    openDates = GetOpenDates(termID),
                                    nonSBUDates = GetNonSBUDates(termID),
                                    gapTime = (int)info.s.GAP_TIME,
                                    reminderInterval = (int)info.s.REMINDER_INTERVAL
                                }).FirstOrDefault();
                return Json(termInfo);
            }
        }

        public JsonResult GetHours(int termID)
        {
            using (var db = new TCSEntities())
            {
                var dates = (from t in db.Term
                             where t.ID == termID
                             select t).AsEnumerable().Select(t => new
                             {
                                 openTimeWeekdayString = DateTime.Now.Date + t.WKDY_OPEN.Value,
                                 closeTimeWeekdayString = DateTime.Now.Date + t.WKDY_CLOSE.Value,
                                 openTimeWeekendString = DateTime.Now.Date + t.WKND_OPEN.Value,
                                 closeTimeWeekendString = DateTime.Now.Date + t.WKND_CLOSE.Value
                             }).FirstOrDefault();
                return Json(dates);
            }
        }

        public JsonResult GetOpenDates(int termID)
        {
            using (var db = new TCSEntities())
            {
                var dates = (from d in db.open_dates
                             where d.TERM_ID == termID && d.SBU_FLG == 1
                             select d).AsEnumerable().Select(d => new
                             {
                                 startDateString = d.START_DATE.Value,
                                 endDateString = d.END_DATE.Value
                             }).FirstOrDefault();
                return Json(dates);
            }
        }

        public JsonResult GetNonSBUDates(int termID)
        {
            using (var db = new TCSEntities())
            {
                var dates = (from d in db.open_dates
                             where d.TERM_ID == termID && d.SBU_FLG == 0
                             select d).AsEnumerable().Select(d => new
                             {
                                 startDateString = d.START_DATE.Value,
                                 endDateString = d.END_DATE.Value
                             }).FirstOrDefault();
                return Json(dates);
            }
        }

        public string EditNumSeats(int numSeats)
        {
            using (var db = new TCSEntities())
            {
                var systemValues = (from s in db.SystemValues
                                    select s).First();
                int origNumSeats = systemValues.NBR_SEATS;
                systemValues.NBR_SEATS = numSeats;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Number of seats successfully changed from " + origNumSeats + " to " + numSeats + ".";
            }
        }

        public string EditNumSetAsideSeats(int numSetAsideSeats)
        {
            using (var db = new TCSEntities())
            {
                var systemValues = (from s in db.SystemValues
                                    select s).First();
                int origNumSeats = (int)systemValues.SET_ASIDE_SEATS;
                systemValues.SET_ASIDE_SEATS = numSetAsideSeats;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Number of set-aside seats successfully changed from " + origNumSeats + " to " + numSetAsideSeats + ".";
            }
        }

        public string EditCenterHours(int termID, DateTime weekdayOpen, DateTime weekdayClose, DateTime weekendOpen, DateTime weekendClose)
        {
            using (var db = new TCSEntities())
            {
                var term = (from t in db.Term
                            where t.ID == termID
                            select t).FirstOrDefault();
                if (term == null)
                    return "Invalid term.";
                var origWeekdayOpen = term.WKDY_OPEN.Value.ToString();
                var origWeekdayClose = term.WKDY_CLOSE.Value.ToString();
                var origWeekendOpen = term.WKND_OPEN.Value.ToString();
                var origWeekendClose = term.WKND_CLOSE.Value.ToString();
                term.WKDY_OPEN = weekdayOpen.TimeOfDay;
                term.WKDY_CLOSE = weekdayClose.TimeOfDay;
                term.WKND_OPEN = weekendOpen.TimeOfDay;
                term.WKND_CLOSE = weekendClose.TimeOfDay;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Hours for " + term.NAME + " successfully changed from " +
                    origWeekdayOpen + " - " + origWeekdayClose + " to " + weekdayOpen.TimeOfDay.ToString() + " - " + weekdayClose.TimeOfDay.ToString() + " on weekdays and from " +
                    origWeekendOpen + " - " + origWeekendClose + " to " + weekendOpen.TimeOfDay.ToString() + " - " + weekendClose.TimeOfDay.ToString() + " on weekends.";
            }
        }

        public string EditOpenDates(int termID, DateTime startDate, DateTime endDate)
        {
            using (var db = new TCSEntities())
            {
                var dates = (from d in db.open_dates
                             where d.TERM_ID == termID && d.SBU_FLG == 1
                             select d).FirstOrDefault();
                if (dates == null)
                    return "Invalid term.";
                var origStartDate = dates.START_DATE.Value.ToShortDateString();
                var origEndDate = dates.END_DATE.Value.ToShortDateString();
                dates.START_DATE = startDate;
                dates.END_DATE = endDate;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Open dates for " + dates.Term.NAME + " successfully changed from " +
                    origStartDate + " - " + origEndDate + " to " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString() + ".";
            }
        }

        public string EditNonSBUDates(int termID, DateTime startDate, DateTime endDate)
        {
            using (var db = new TCSEntities())
            {
                var dates = (from d in db.open_dates
                             where d.TERM_ID == termID && d.SBU_FLG == 0
                             select d).FirstOrDefault();
                if (dates == null)
                    return "Invalid term.";
                var origStartDate = dates.START_DATE.Value.ToShortDateString();
                var origEndDate = dates.END_DATE.Value.ToShortDateString();
                dates.START_DATE = startDate;
                dates.END_DATE = endDate;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Reserved non-SBU dates for " + dates.Term.NAME + " successfully changed from " +
                    origStartDate + " - " + origEndDate + " to " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString() + ".";
            }
        }

        public string EditGapTime(int gapTime)
        {
            using (var db = new TCSEntities())
            {
                var systemValues = (from s in db.SystemValues
                                    select s).First();
                int origGapTime = (int)systemValues.GAP_TIME;
                systemValues.GAP_TIME = gapTime;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Gap time successfully changed from " + origGapTime + " minutes to " + gapTime + " minutes.";
            }
        }

        public string EditReminderInterval(int reminderInterval)
        {
            using (var db = new TCSEntities())
            {
                var systemValues = (from s in db.SystemValues
                                    select s).First();
                int origRI = (int)systemValues.REMINDER_INTERVAL;
                systemValues.REMINDER_INTERVAL = reminderInterval;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Reminder interval successfully changed from " + origRI + " minutes to " + reminderInterval + " minutes.";
            }
        }
        #endregion

        #region ImportData
        public JsonResult GetTerms()
        {
            return Json(TermService.GetTerms());
        }
        #endregion

        #region Reports
        public JsonResult StudentAppointmentsInTermDaily(int termID)
        {
            using (var db = new TCSEntities())
            {
                // Get all appointments in date range of term
                var appointments = (from a in db.Appointment
                                    join e in db.Exam on a.ExamID equals e.ID
                                    where e.TERM_ID == termID
                                    select a).AsEnumerable();
                var dates = db.open_dates.Where(d => d.TERM_ID == termID && d.SBU_FLG == 1).FirstOrDefault();
                if (!appointments.Any() || dates == null)
                    return Json(new string[] { });
                List<dynamic> result = new List<dynamic>();
                // For each date in the term get student appointments on that day
                for (var date = dates.START_DATE.Value; date < dates.END_DATE.Value.AddDays(1); date = date.AddDays(1))
                    result.Add(new { date = date.ToShortDateString(), appointments = appointments.Count(a => a.APPT_TIME.Value.Date.Equals(date.Date)) });
                return Json(result);
            }
        }

        public JsonResult CoursesInTerm(int termID)
        {
            using (var db = new TCSEntities())
            {
                // Get the courses for which there existed appointments in the given term
                var courses = (from c in db.Class
                               join e in db.Exam on c.ID equals e.classID
                               join a in db.Appointment on e.ID equals a.ExamID
                               where e.TERM_ID == termID
                               select new { course = c.Subject + " " + c.CatalogID}).Distinct().ToList();
                return Json(courses);
            }
        }

        public JsonResult StudentAppointmentCountOverTerms(int startTermID, int endTermID)
        {
            using (var db = new TCSEntities())
            {
                if (startTermID > endTermID)
                    return null;
                var term = (from a in db.Appointment
                            join e in db.Exam on a.ExamID equals e.ID
                            where e.TERM_ID >= startTermID && e.TERM_ID <= endTermID
                            select e).AsEnumerable();
                List<dynamic> result = new List<dynamic>();
                for (int tCounter = startTermID; tCounter <= endTermID; tCounter++)
                    result.Add(new { term = db.Term.Find(tCounter).NAME, appointments = term.Count(a => a.TERM_ID == tCounter) });
                return Json(result);
            }
        }
        #endregion

        #region ScheduleExamForStudent
        public JsonResult GetStudentExams(string studentID)
        {
            using (var db = new TCSEntities())
            {
                var exams = ExamService.getUpcomingExamsByUser(studentID);
                return Json(exams);
            }
        }

        /// <summary>
        /// Loads the exam data for the Make Appointment page
        /// </summary>
        /// <returns></returns>
        public JsonResult loadMkAppData()
        {
            int currentExam = (int)HttpContext.Session[Constants.SessionStrings.CURRENT_EXAM];
            var db2 = new TCSEntities();
            using (var db = new TCSEntities())
            {
                var user = UserService.GetSignedInUser();
                if (user == null)
                {
                    Logger.LogWarning("There is no signed in user.");
                    return null;
                }
                var exam = from e in db.Exam
                           where e.ID == currentExam
                           select new { e.ID, e.NAME, e.StartTime, e.EndTime, e.classID, e.DURATION };
                var ex = exam.AsEnumerable()
                    .Select(e => new
                    {
                        id = e.ID,
                        name = e.NAME,
                        start = e.StartTime.ToString(),
                        end = e.EndTime.ToString(),
                        classID = e.classID,
                        duration = e.DURATION,
                        className = db2.Class.Where(c => c.ID == e.classID).Select(m => new { sub = m.Subject, catNo = m.CatalogID }).FirstOrDefault(),
                    }).FirstOrDefault();
                var term = from c in db.Class
                           where c.ID == ex.classID
                           select c.Term;

                var hours = term.AsEnumerable()
                    .Select(h => new
                    {
                        wkndOpen = h.WKND_OPEN.ToString(),
                        wkndClose = h.WKND_CLOSE.ToString(),
                        wkdyOpen = h.WKDY_OPEN.ToString(),
                        wkdyClose = h.WKDY_CLOSE.ToString()
                    }
                        ).FirstOrDefault();

                return Json(new { ex = ex, hours = hours });
            }
        }

        /// <summary>
        /// Attempts to make an appointment
        /// </summary>
        /// <returns>the appointment, whether it was approved or not</returns>
        public bool RequestAppointmentForStudent(DateTime startTime, bool setAsideSeat)
        {
            var user = UserService.GetSignedInUser();
            string studentID;
            int examID;
            try
            {
                studentID = (string)Session[Constants.SessionStrings.STUDENT_ID];
                examID = (int)Session[Constants.SessionStrings.CURRENT_EXAM];
            }
            catch(Exception e)
            {
                return false;
            }
            if (user == null)
            {
                Logger.LogWarning("There is no signed in user.");
                return false;
            }
            var approved = AppointmentService.requestAppointment(startTime, examID, user.netID, studentID, setAsideSeat);

            return approved;
        }
        #endregion

        #region Utilization
        /// <summary>
        /// Gets a (dateStr,double) map of utilizations within the range
        /// </summary>
        public JsonResult GetUtilization(int termID, DateTime startDate, DateTime endDate)
        {
            var utilMap = UtilizationService.UtilizationsForDateRange(termID, startDate, endDate);
            return Json(utilMap);
        }

        /// <summary>
        /// Get utilizations in range if we hypothetically added this exam
        /// </summary>
        public JsonResult GetUtilizationHypothetical(int termID, DateTime startDate, DateTime endDate, int examID)
        {
            // TODO move the heavy logic into service class
            using (var db = Database.Context)
            {
                var e = ExamService.Load(examID);
                // APPROVE AND SAVE SO UTIL ALGO PICKS IT UP
                e.Approved = 1;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }

                try
                {
                    var utilMap = UtilizationService.UtilizationsForDateRange(termID, startDate, endDate);
                    return Json(utilMap);
                }
                finally
                {
                    // DISAPPROVE AND SAVE
                    e.Approved = 0;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                        throw;
                    }
                }
            }
        }
        #endregion

        #region General
        /// <summary>
        /// Gets the terms and user info and is called on each page load
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBasicInfo()
        {
            using (var db = new TCSEntities())
            {
                var terms = (from t in db.Term
                             join d in db.open_dates on t.ID equals d.TERM_ID
                             where d.SBU_FLG == 1
                             select new { t, d }).AsEnumerable().Select(data => new
                             {
                                 id = data.t.ID,
                                 name = data.t.NAME,
                                 startDate = data.d.START_DATE.ToString(),
                                 endDate = data.d.END_DATE.ToString()
                             }).ToList();
                var user = UserService.GetSignedInUser();
                if (user == null)
                {
                    Logger.LogWarning("There is no signed in user.");
                    return null;
                }
                return Json(new { terms = terms, user = user });
            }
        }
        #endregion
    }
}
