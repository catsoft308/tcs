﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TCS.Services;
using TCS.App_Data;

namespace TCS.Controllers
{
    public class StudentsController : Controller
    {
        public ActionResult Home()
        {
            if (UserService.UserHasRole(UserService.STUDENT_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }

        public ActionResult MakeAppointment(int examID)
        {
            if (UserService.UserHasRole(UserService.STUDENT_ROLE))
            {
                System.Web.HttpContext.Current.Session[Constants.SessionStrings.CURRENT_EXAM] = examID;
                return View();
            }
            return RedirectToAction("Login", "Home");
        }


        /// <summary>
        /// Gets the terms and user info and is called on each page load
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBasicInfo()
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var terms = (from t in db.Term
                             select new { name = t.NAME, id = t.ID }).ToList();
                var user = UserService.GetSignedInUser();
                if (user == null)
                {
                    Logger.LogWarning("There is no signed in user.");
                    return null;
                }
                return Json(new { terms = terms, user = user });
            }
        }

        /// <summary>
        /// Loads the exam and appointment data for the home page
        /// </summary>
        /// <returns></returns>
        public JsonResult loadHomeData()
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var user = UserService.GetSignedInUser();
                if (user == null)
                {
                    Logger.LogWarning("There is no signed in user.");
                    return null;
                }
                var exams = ExamService.getUpcomingExamsByUser(user.netID);
                var previous = AppointmentService.getPreviousApptsByUser(user.netID);
                var upcoming = AppointmentService.getUpcomingApptsByUser(user.netID);
                return Json(new { exams = exams, upcoming = upcoming, previous = previous });
            }
        }

        /// <summary>
        /// Loads the exam data for the Make Appointment page
        /// </summary>
        /// <returns></returns>
        public JsonResult loadMkAppData()
        {
            int currentExam = (int)HttpContext.Session[Constants.SessionStrings.CURRENT_EXAM];
            var db2 = new TCS.DAL.TCSEntities();
            using (var db = new TCS.DAL.TCSEntities())
            {
                var user = UserService.GetSignedInUser();
                if (user == null)
                {
                    Logger.LogWarning("There is no signed in user.");
                    return null;
                }
                var exam = from e in db.Exam
                           where e.ID == currentExam
                           select new { e.ID, e.NAME, e.StartTime, e.EndTime, e.classID, e.DURATION };
                var ex = exam.AsEnumerable()
                    .Select(e => new
                    {
                        id = e.ID,
                        name = e.NAME,
                        start = e.StartTime.ToString(),
                        end = e.EndTime.ToString(),
                        classID = e.classID,
                        duration = e.DURATION,
                        className = db2.Class.Where(c => c.ID == e.classID).Select(m => new { sub = m.Subject, catNo = m.CatalogID }).FirstOrDefault(),
                    }).FirstOrDefault();
                var term = from c in db.Class
                    where c.ID == ex.classID
                    select c.Term;

                var hours = term.AsEnumerable()
                    .Select(h => new
                    {
                        wkndOpen = h.WKND_OPEN.ToString(),
                        wkndClose = h.WKND_CLOSE.ToString(),
                        wkdyOpen = h.WKDY_OPEN.ToString(),
                        wkdyClose = h.WKDY_CLOSE.ToString()
                    }
                        ).FirstOrDefault();
                            
                return Json(new { ex = ex , hours = hours});
            }
        }

        /// <summary>
        /// Attempts to make an appointment
        /// </summary>
        /// <returns>the appointment, whether it was approved or not</returns>
        public bool RequestAppointment(DateTime startTime, int exam_id, string appointment_owner, string created_by)
        {
            var user = UserService.GetSignedInUser();
            if (user == null)
            {
                Logger.LogWarning("There is no signed in user.");
                return false;
            }
            var approved = AppointmentService.requestAppointment(startTime, exam_id, created_by, appointment_owner, false);

            return approved;
        }

        /// <summary>
        /// Cancels the selected appointment
        /// </summary>
        /// <returns>whether the appointment was canceled</returns>
        public bool CancelAppointment(int apptID)
        {
            var user = UserService.GetSignedInUser();
            if (user == null)
            {
                Logger.LogWarning("There is no signed in user.");
                return false;
            }
            var approved = AppointmentService.cancelAppointment(apptID);

            return approved;
        }
    }
}
