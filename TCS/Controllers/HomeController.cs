﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TCS.Services;
using TCS.App_Data;
using System.Web.Security;
using WebMatrix.WebData;

namespace TCS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        
        public String userLogin(String username, String password)
        {
            return UserService.userLogin(username, password);
        }

        public ActionResult logout()
        {
            Logger.LogInfo("User logged out: " + HttpContext.Session[Constants.SessionStrings.USER_ID]);
            HttpContext.Session[Constants.SessionStrings.ROLES] = null;
            HttpContext.Session[Constants.SessionStrings.USER_ID] = null;
            return RedirectToAction("Login");
        }
    }
}
