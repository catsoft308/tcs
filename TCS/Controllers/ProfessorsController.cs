﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TCS.Services;

namespace TCS.Controllers
{
    public class ProfessorsController : Controller
    {
        public ActionResult Home()
        {
            if (UserService.UserHasRole(UserService.PROFESSOR_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult ScheduleExam()
        {
            if (UserService.UserHasRole(UserService.PROFESSOR_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }
        public ActionResult ScheduleAdHocExam()
        {
            if (UserService.UserHasRole(UserService.PROFESSOR_ROLE))
                return View();
            return RedirectToAction("Login", "Home");
        }

        /// <summary>
        /// Gets the terms and user info and is called on each page load
        /// </summary>
        /// <returns></returns>
        public JsonResult GetScheduleExamInfo(int sbu)
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var terms = (from t in db.Term
                             join d in db.open_dates on t.ID equals d.TERM_ID
                             where d.SBU_FLG == sbu
                             select new { t, d }).AsEnumerable().Select(data => new
                             {
                                 id = data.t.ID,
                                 name = data.t.NAME,
                                 startDate = data.d.START_DATE.ToString(),
                                 endDate = data.d.END_DATE.ToString()
                             }).ToList();
                var user = UserService.GetSignedInUser();
                if (user == null)
                    return null;
                return Json(new { terms = terms, user = user });
            }
        }

        public JsonResult getClassesByTerm(int termID, string netID)
        {
            var classes = ClassService.getClassesByTerm(termID, netID);

            return Json(new { classes = classes });
        }
        public Boolean scheduleAnExam(string name, int duration, DateTime startTime, DateTime endTime, int termID, string netID, int classID)
        {
            var resp = ExamService.scheduleExam(name, duration, startTime, endTime, termID, netID, classID);
            return resp;
        }
        public JsonResult generateCalendarEvents(int duration, int term, int sbu)
        {
            List<dynamic> events = new List<dynamic>();

            using (var db = new TCS.DAL.TCSEntities())
            {
                var now = RoundUp(DateTime.Now, TimeSpan.FromMinutes(30));
                var end = from t in db.open_dates
                          where t.TERM_ID == term
                          && t.SBU_FLG == sbu
                          select  t.END_DATE;

                var End = end.AsEnumerable().Select(t => new
                {
                    date = t.Value
                }).First();

                DateTime endTime = End.date;

                var currentTerm = from t in db.Term
                                  where t.ID == term
                                  select new { t.WKDY_OPEN, t.WKDY_CLOSE, t.WKND_OPEN, t.WKND_CLOSE };
                var termTimes = currentTerm.AsEnumerable().Select(tt => new
                {
                    wkdyOpen = tt.WKDY_OPEN,
                    wkdyClose = tt.WKDY_CLOSE,
                    wkndOpen = tt.WKND_OPEN,
                    wkndClose = tt.WKND_CLOSE
                }).First();

                if(endTime.DayOfWeek == DayOfWeek.Saturday || endTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    endTime = endTime.AddHours(((TimeSpan)termTimes.wkndClose).Hours);
                    endTime = endTime.AddMinutes(((TimeSpan)termTimes.wkndClose).Minutes);
                }
                else
                {
                    endTime = endTime.AddHours(((TimeSpan)termTimes.wkdyClose).Hours);
                    endTime = endTime.AddMinutes(((TimeSpan)termTimes.wkdyClose).Minutes);
                }

                DateTime next = now;

                while(next < endTime)
                {
                    TimeSpan open = (next.DayOfWeek == DayOfWeek.Saturday || next.DayOfWeek == DayOfWeek.Sunday) ? ((TimeSpan)termTimes.wkndOpen) : ((TimeSpan)termTimes.wkdyOpen);
                    TimeSpan close = (next.DayOfWeek == DayOfWeek.Saturday || next.DayOfWeek == DayOfWeek.Sunday) ? ((TimeSpan)termTimes.wkndClose) : ((TimeSpan)termTimes.wkdyClose);

                    if(next.TimeOfDay >= open && next.TimeOfDay < close.Subtract(TimeSpan.FromMinutes(duration)))
                    {
                        int apptsAtTime = AppointmentService.getNumApptsAvailAtTime(next, db);
                        events.Add(new { title = apptsAtTime, start = getCalendarFormat(next), end = getCalendarFormat(next.AddMinutes(30)) });
                        Console.WriteLine("event added for " + next.ToShortDateString());
                    }
                    next = next.AddMinutes(30);
                }


                dynamic selectable = new {
                    open = pad(((TimeSpan)termTimes.wkdyOpen).Hours.ToString()) + ":" + pad(((TimeSpan)termTimes.wkdyOpen).Minutes.ToString()),
                    close = pad(((TimeSpan)termTimes.wkdyClose).Hours.ToString()) + ":" + pad(((TimeSpan)termTimes.wkdyClose).Minutes.ToString())

                };
                

                return Json(new {events = events, selectable = selectable });



            }
        }

        private string getCalendarFormat(DateTime date)
        {
            return date.Year + "-" + pad(date.Month.ToString()) + "-" + pad(date.Day.ToString()) + "T" + pad(date.Hour.ToString()) + ":" + pad(date.Minute.ToString()) + ":" + pad(date.Second.ToString());
        }
        private string pad(string num)
        {
            string s = num + "";
            while (s.Length < 2) s = "0" + s;
            return s;
        }
        
        public JsonResult GetExams(int termID)
        {
            var user = UserService.GetSignedInUser();
            if (user == null)
            {
                Logger.LogWarning("There is no signed in user.");
                return null;
            }
            string userID = user.netID;
            using (var db = new TCS.DAL.TCSEntities())
            {
                var exams = (from e in db.Exam
                             join c in db.Class on e.classID equals c.ID
                             where e.TERM_ID == termID && e.CreatedBy == userID
                             select new { e, c }).AsEnumerable().Select(data => new
                             {
                                courseID = data.c.Subject + " " + data.c.CatalogID,
                                id=data.e.ID,
                                name = data.e.NAME,
                                startTime = data.e.StartTime.Value.ToShortDateString() + " at " + data.e.StartTime.Value.TimeOfDay.ToString(),
                                endTime = data.e.EndTime.Value.ToShortDateString() + " at " + data.e.EndTime.Value.TimeOfDay.ToString(),
                                duration= data.e.DURATION,
                                approved = data.e.Approved == 1
                            }).ToList();
                return Json(exams);
            }
        }

        public JsonResult GetExamInfo(int examID)
                {
            using (var db = new TCS.DAL.TCSEntities())
            {
                // Get all student appointments for the given exam
                var appointments = (from a in db.Appointment
                                    where a.ExamID == examID
                                    select a).AsEnumerable().Select(a => new
                                    {
                                        id = a.ID,
                                        seatNumber = a.AssignedSeat,
                                        studentID = a.OwnerID,
                                        time = a.APPT_TIME.Value,
                                        timeString = a.APPT_TIME.Value.ToShortDateString() + " at " + a.APPT_TIME.Value.ToShortTimeString(),
                                        attended = a.CheckedIn == 1
                                       
                });
                return Json(appointments.ToList());
            }
            }

        public string CancelExam(int examID)
        {

            using (var db = new TCS.DAL.TCSEntities())
            {
                var exam = db.Exam.Find(examID);
                var appointments = (from a in db.Appointment
                                    where a.ExamID == examID
                                    select a);
                if (appointments.Any())
                {
                    return "You cannot cancel this exam because students are scheduled to take it.";
                }
                db.Exam.Remove(exam);
                                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return "Exam sucessfully cancelled";
            }
        }
                

       
        DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }


        /// <summary>
        /// Gets the terms and user info and is called on each page load
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBasicInfo()
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var terms = (from t in db.Term
                             join d in db.open_dates on t.ID equals d.TERM_ID
                             where d.SBU_FLG == 1
                             select new { t, d }).AsEnumerable().Select(data => new
                             {
                                 id = data.t.ID,
                                 name = data.t.NAME,
                                 startDate = data.d.START_DATE.ToString(),
                                 endDate = data.d.END_DATE.ToString()
                             }).ToList();
                var user = UserService.GetSignedInUser();
                if (user == null)
                {
                    Logger.LogWarning("There is no signed in user.");
                    return null;
                }
                return Json(new { terms = terms, user = user });
            }
        }
    }
}

