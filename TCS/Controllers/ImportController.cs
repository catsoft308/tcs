﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TCS.CSVImport;
using TCS.Services;

namespace TCS.Controllers
{
    [RoutePrefix("api/Import")]
    public class ImportController : ApiController
    {
        private ICsvImportManager _csvImportManager;

        public ImportController()
            : this(new LocalCsvManager(HttpRuntime.AppDomainAppPath + @"\temp"))
        {            
        }

        public ImportController(ICsvImportManager photoManager)
        {
            _csvImportManager = photoManager;
        }
        
        // POST: api/Import
        public async Task<IHttpActionResult> Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return BadRequest("Unsupported media type");
            }

            try
            {
                Logger.LogInfo("Beginning Import");
                var start = DateTime.Now;

                // do the import
                var csvs = await _csvImportManager.UploadAndImport(Request);

                var elapsed = (DateTime.Now - start).TotalSeconds;
                Logger.LogInfo("Import complete after " + elapsed + " seconds.");

                return Ok(new { Message = "CSVs uploaded ok", Csvs = csvs });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.GetBaseException().Message);
                return BadRequest(ex.GetBaseException().Message);
            }

        }
    }
}