﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="homeTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="homeContent" ContentPlaceHolderID="MainContent" runat="server">
<%: Styles.Render("~/Content/Shared/css") %>
<!DOCTYPE html>
<html ng-app="TCS" ng-controller="StudentHomeController">
<body>

<!-- Main Content Panel -->
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Welcome, {{user.firstName}}</h2>
    </div>
    <div class="panel-body panel-body-main">

        <!-- Upcoming Exams Panel -->
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h3 class="panel-title">Upcoming Exams</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in exams track by model.id" ng-hide="loading">
                        <td>
                            {{model.className.sub}} {{model.className.catNo}}
                        </td>
                        <td>
                            {{model.name}}
                        </td>
                        <td>
                            {{model.start}}
                        </td>
                        <td>
                            {{model.end}}
                        </td>
                        <td>
                            <button type="button" ng-click="makeAppointment(model.id)" class="btn btn-success">Sign Up</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>

        <!-- Upcoming Appontments Panel-->
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h3 class="panel-title">Upcoming Appointments</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Appointment Time</th>
                            <th>Seat Number</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in upcomingAppts track by model.apptTime">
                        <td>
                            {{model.className.sub}} {{model.className.catNo}}
                        </td>
                        <td>
                            {{model.examName}}
                        </td>
                        <td>
                            {{model.apptTime}}
                        </td>
                        <td>
                            {{model.seatNumber}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" ng-click="setCurrentAppt(model)" data-toggle="modal" data-target="#CancelConf">Cancel</button>
                        </td>

                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>

        <!-- Previous Appointments Panel -->
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h3 class="panel-title">Previous Appointments</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Appointment Time</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in prevAppts track by model.apptTime">
                        <td>
                            {{model.className.sub}} {{model.className.catNo}}   
                        </td>
                        <td>
                            {{model.examName}}
                        </td>
                        <td>
                            {{model.apptTime}}
                        </td>
                        <td>
                            {{model.status}}
                        </td>
                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>
    </div>

    <!--  Confirm Cancellation Modal -->
    <div class="modal fade" id="CancelConf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Confirm Cancellation</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to cancel your appointment for the {{currentAppt.courseID}} exam {{currentAppt.examName}}? Once cancelled, it cannot be restored.
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" ng-click="cancelAppointment(currentAppt.courseID)"  data-dismiss="modal" data-toggle="modal" data-target="#Cancelled">Cancel</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Nevermind</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Cancellation Notification Modal -->
    <div class="modal fade" id="Cancelled" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Appointment Cancelled</h4>
          </div>
          <div class="modal-body">
            <p>Your Appointment for the {{currentAppt.courseID}} exam {{currentAppt.examName}}:</p>
              <h5>{{currentAppt.apptTime}}</h5>
            <p>has been cancelled.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</div>
</body>
</html>
</asp:Content>
