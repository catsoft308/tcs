﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="homeTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Schedule Ad Hoc Exam - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="homeContent" ContentPlaceHolderID="MainContent" runat="server">
        <%: Styles.Render("~/Content/Shared/css") %>








<!DOCTYPE html>


            <link href='../../fullcalendar/fullcalendar.css' rel='stylesheet' />
        <link href='../../fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
        <script src='../../fullcalendar/lib/moment.min.js'></script>

        <script src='../../fullcalendar/lib/jquery.min.js'></script>
    <script src='../../fullcalendar/fullcalendar.min.js'></script>
<script>

    $(document).ready(function () {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
           eventBackgroundColor:'white',
           defaultView: 'agendaWeek',
            eventTextColor:'black',
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
                            {
                                title: 'Nothing lol',
                                start: '2015-02-03T13:00:00',
                                constraint: 'businessHours'
                            },
                            {
                                title: 'Meeting',
                                start: '2015-02-13T11:00:00',
                                constraint: 'availableForMeeting', // defined below
                                color: '#257e4a'
                            },
                            {
                                title: 'Conference',
                                start: '2015-02-18',
                                end: '2015-02-20'
                            },
                            {
                                title: 'Party',
                                start: '2015-02-29T20:00:00'
                            },



            ],
            loading: function (bool) {
                $('#loading').toggle(bool);
            }
        });





    });

</script>





<html ng-app="TCS" ng-controller="ProfessorHomeController">
<body>
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Schedule Ad Hoc Exam</h2>
    </div>
    <div class="panel-body panel-body-main">
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h3 class="panel-title">Schedule Exam</h3>
            </div>
            <div class="panel-body panel-body-inner">
                   <div class="panel-body panel-body-main">
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-body panel-body-inner">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            Exam Name:
                        </td>
                        <td>
                            <input type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Course ID:
                        </td>
                        <td>
                            <input type="text" />

                        </td>
                    </tr>                    <tr>
                        <td>
                            Section Number:
                        </td>
                        <td>
                            <input type="text" />

                        </td>
                    </tr>                    <tr>
                        <td>
                            Term:
                        </td>
                        <td>
                            <input type="text" />

                        </td>
                    </tr>

                            <button type="button" class="btn btn-danger"  data-dismiss="modal">Cancel</button>
                            <button type="button" ng-click="setCurrentExam(model)" class="btn btn-success" data-toggle="modal" data-target="#SubmitExam">Submit</button>

                </table>
            </div>
        </div>
    </div>


</div>
</div>





    </div>


        <div class="panel-body panel-body-main">
	<div id='calendar' eventBackgroundColor="red"></div>
     </div>
    <!-- Modal -->
    <div class="modal fade" id="SubmitExam" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Confirm</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to add this exam?
          </div>

          <div class="modal-footer">

              <button type="button" class="btn btn-danger"  data-dismiss="modal">Continue Editing</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Yes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</body>
</html>
</asp:Content>
