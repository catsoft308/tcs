﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="homeTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="homeContent" ContentPlaceHolderID="MainContent" runat="server">
        <%: Styles.Render("~/Content/Shared/css") %>

<!DOCTYPE html>
<html ng-app="TCS" ng-controller="ProfessorHomeController">
<body>
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Welcome {{user.firstName}}!</h2>
    </div>
    <div class="panel-body panel-body-main">
        <div class="panel panel-default box-shadow--2dp">
            <div>
                <select ng-model="currentTerm" ng-options="term as term.name for term in terms"></select>
            </div>
            <div class="panel-heading">
                <h3 class="panel-title">Upcoming Exams</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in exams " ng-hide="loading">
                        <td>
                            {{model.courseID}}
                        </td>
                        <td>
                            {{model.name}}
                        </td>
                        <td>
                            {{model.startTime}}
                        </td>
                        <td>
                            {{model.endTime}}
                        </td>
                        <td ng-show="model.approved">
                            <i>Approved</i>
                        </td>
                        <td ng-hide="model.approved">
                            <i>Pending</i>
                        </td>
                        <td>
                            <button type="button" ng-click="setCurrentExam(model)" class="btn btn-danger" data-toggle="modal" data-target="#CancelExam">Cancel</button>
                        </td>
                        <td>
                            <button type="button" ng-click="seeExamInfo(model)" class="btn btn-default" data-toggle="modal" data-target="#ExamInfo">Info</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>
        <button type="button" ng-click="scheduleExam()" class="btn btn-default">Schedule an Exam</button>
        <button type="button" ng-click="scheduleAdHocExam()" class="btn btn-default">Schedule an Ad Hoc Exam</button>
    </div>

        <!-- Cancel Appointment Modal -->
    <div class="modal fade" id="CancelExam" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cancel Exam</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to cancel {{currentExam.courseID + " " + currentExam.name}}?</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" ng-click="cancelExam()">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Exam Info Modal -->
    <div class="modal fade" id="ExamInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Info for {{currentExam.courseID + " " + currentExam.name}}</h4>
          </div>
          <div class="modal-body">
            <p>Exam Time: {{currentExam.startTime + "-" + currentExam.endTime}}</p>
            <p>Exam Duration: {{currentExam.duration}} minutes</p>
            <div class="panel panel-default box-shadow--2dp">
                <div class="panel-heading">
                    <h3 class="panel-title">Scheduled</h3>
                </div>
                <div class="panel-body panel-body-inner">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Id</td>
                                <td>Time</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tr ng-repeat="model in studentAppointments track by model.studentID" ng-hide="modalLoading">
                            <td>
                                {{model.studentID}}
                            </td>
                            <td>
                                {{model.timeString}}
                            </td>

                            <td ng-hide="model.attended">
                                <i>Pending</i>
                            </td>
                            <td ng-show="model.attended">
                                <i>Attended</i>
                            </td>
                        </tr>
                    </table>
                </div>
                <div ng-show="modalLoading">
                    Loading...
                </div>
            </div>

          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</body>
</html>
</asp:Content>
