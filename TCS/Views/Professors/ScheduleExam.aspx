﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="homeTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Schedule Exam - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="homeContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Styles.Render("~/Content/Shared/css") %>

    <!DOCTYPE html>

    <link href='../../fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link href='../../fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='../../fullcalendar/lib/moment.min.js'></script>

    <script src='../../fullcalendar/lib/jquery.min.js'></script>
    <script src='../../fullcalendar/fullcalendar.min.js'></script>
    <script>


    </script>


    <html ng-app="TCS" ng-controller="ScheduleExamController">
    <body>
        <!-- Main Content Panel-->
        <div class="panel panel-main panel-default box-shadow--6dp">
            <div class="panel-heading panel-heading-main">
                <h2 class="panel-title">Request Exam</h2>
            </div>

            <!-- Exam info Panel-->
            <div class="panel-body panel-body-main">

                <form class="form-inline">
                    <div class="form-group col-md-5">
                        <div class="col-md-5">
                            <label for="#termInput">Term</label>
                        </div>
                        <div class="col-md-5">
                            <select id="termInput" ng-model="currentTerm" ng-options="term as term.name for term in terms" ng-change="getClassesForTerm()"></select>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <div class="col-md-5">
                            <label for="#classInput">Class</label>
                        </div>
                        <div class="col-md-5">
                            <select id="classInput" ng-model="currentClass" ng-options="class as class.name for class in classes"></select>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <div class="col-md-5">
                            <label for="#nameInput">Exam Name</label>
                        </div>
                        <div class="col-md-5">
                            <input id="nameInput" ng-model="exam.name" type="text">
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <div class="col-md-5">
                            <label for="#durationInput">Duration</label>
                        </div>
                        <div class="col-md-5">
                            <input id="durationInput" ng-model="exam.duration" type="number" step="5" min="0" onkeydown="event.preventDefault()">
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <div class="col-md-5">
                            <label for="#startInput">Start Time</label>
                        </div>
                        <div class="col-md-5">
                            <p id="startInput">{{exam.startTime.toDateString() + " " + exam.startTime.toTimeString().substring(0,8)}}</p>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <div class="col-md-5">
                            <label for="#endInput">End Time</label>
                        </div>
                        <div class="col-md-5">
                            <p id="endInput">{{exam.endTime.toDateString() + " " + exam.endTime.toTimeString().substring(0,8)}}</p>
                        </div>
                    </div>

                    <div class="col-md-20">
                        <div class="col-md-10">
                            <button type="button" ng-disabled="exam.duration == ''" class="btn btn-warning" ng-click="generateCalendarEvents()" data-toggle="modal" data-target="#ApptNotif">Populate Schedulability</button>
                        </div>
                        <div class="col-md-2">
                            <button type="button" ng-disabled="exam.startTime == '' || exam.endTime == ''" class="btn btn-success" ng-click="scheduleAnExam()" data-toggle="modal" data-target="#ExamNotif">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div ng-if="loading">Loading. This may take a minute.</div>

            <div id='calendar' eventbackgroundcolor="red"></div>

        </div>



        <!-- Appointment Request Response Modal -->
    <div class="modal fade" id="ExamNotif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-show="successfulSchedule" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Request Submitted.</h4>
          </div>
          <div class="modal-body">
            <p>Your Exam request for the following dates:</p>
              <h5>Start: {{exam.startTime.toDateString()}} at {{exam.startTime.toTimeString()}}</h5>
              <h5>Start: {{exam.endTime.toDateString()}} at {{exam.endTime.toTimeString()}}</h5>

            <p>has been submitted.</p>
          </div>
          <div class="modal-footer">
            <button ng-show="successfulSchedule" onclick="location.href='Home'" type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
            <button ng-show="!successfulSchedule" type="button" class="btn btn-default" data-dismiss="modal" >Close</button>

          </div>
        </div>
      </div>
    </div>


    </body>
    </html>
</asp:Content>
