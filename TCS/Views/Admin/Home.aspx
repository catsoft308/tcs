﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<%: Styles.Render("~/Content/Shared/css") %>
<!DOCTYPE html>
<html ng-app="TCS" ng-controller="AdminHomeController">

<!-- Main Content Panel -->
<body>
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Welcome, {{user.firstName}}!</h2>
    </div>
    <div class="panel-body panel-body-main">
        <div>
            <h3><a href="Exams">Exams</a><br /></h3>
            <h3><a href="Utilization">Utilization</a><br /></h3>
            <h3><a href="ImportData">Import Data</a><br /></h3>
            <h3><a href="ScheduleExamForStudent">Schedule an Exam for a Student</a><br /></h3>
            <h3><a href="EditCenterInfo">Edit Testing Center Information</a><br /></h3>
            <h3><a href="Reports">Reports</a></h3>
        </div>
    </div>
</div>
</body>
</html>
</asp:Content>
