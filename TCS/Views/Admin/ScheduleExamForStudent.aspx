﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Schedule Exam For Student - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<%: Styles.Render("~/Content/Shared/css") %>
<!DOCTYPE html>
<html ng-app="TCS" ng-controller="ScheduleExamForStudentController">
<body>

<!-- Main Content Panel -->
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Schedule an Exam for a Student</h2>
    </div>
    <div class="panel-body panel-body-main">
        <div>
            <span>Student ID: <input type="text" ng-model="studentID" /></span>
            <br />
            <button type="button" class="btn btn-success" ng-click="getStudent()">Submit</button>
            <button type="button" id="invalidIDBtn" style="display:none;" data-toggle="modal" data-target="#NoMatchModal"></button>
        </div>

        <!-- Exams for Student Panel -->
        <div class="panel panel-default box-shadow--2dp" ng-show="studentExams.length > 0">
            <div class="panel-heading">
                <h3 class="panel-title">Upcoming Exams</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in studentExams track by model.id" ng-hide="loading">
                        <td>
                            {{model.className.sub + " " + model.className.catNo}}
                        </td>
                        <td>
                            {{model.name}}
                        </td>
                        <td>
                            {{model.start}}
                        </td>
                        <td>
                            {{model.end}}
                        </td>
                        <td>
                            <button type="button" ng-click="makeAppointment(model)" class="btn btn-success">Sign Up</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>
    </div>

    <!-- Invalid Student ID Notification Modal -->
    <div class="modal fade" id="NoMatchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Invalid Student ID</h4>
          </div>
          <div class="modal-body">
            <p>Could not find a student with the id {{studentID}}</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</body>
</html>
</asp:Content>
