﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Utilization - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Styles.Render("~/Content/Shared/css") %>
    <!DOCTYPE html>
    <html ng-app="TCS" ng-controller="UtilizationController">
    <body>

        <!-- Main Content Panel -->
        <div class="panel panel-main panel-default box-shadow--6dp">
            <div class="panel-heading panel-heading-main">
                <h2 class="panel-title">Center Utilization</h2>
            </div>
            <div class="panel-body panel-body-main">

                <form class="form-inline">
                    <div>
                        <select ng-model="selectedTerm" ng-options="term as term.name for term in terms" ng-change="configPickers()"></select>
                    </div>
                    <div class="form-group col-md-4">
                        From:
                        <!-- Start Date picker -->
                        <p class="input-group">
                            <input type="text" readonly onkeydown="event.preventDefault()" class="form-control" uib-datepicker-popup="{{format}}" show-weeks="false" ng-model="startDate" is-open="openedStart" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" ng-change="generateTime()" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="toggleOpenStart()"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </p>                    </div>
                    <div class="form-group col-md-4">
                        To:
                        <!-- End Date picker -->
                        <p class="input-group">
                            <input type="text" readonly onkeydown="event.preventDefault()" class="form-control" uib-datepicker-popup="{{format}}" show-weeks="false" ng-model="endDate" is-open="openedEnd" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" ng-change="generateTime()" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="toggleOpenEnd()"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success" ng-click="getUtilization()">See Utilization</button>
                    </div>
                </form>

                <!-- Utilization Graph Panel -->
                <div class="panel panel-default box-shadow--2dp" ng-show="!isEmptyObject(utilization)">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Utilization</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="(date, util) in utilization" ng-hide="loading">
                            <td>{{date}}</td>
                            <td>{{util | number : 2}}%</td>
                        </tr>
                    </table>
                </div>
                <div ng-show="loading">
                    Loading...
       
                </div>
            </div>
        </div>
    </body>
    </html>
</asp:Content>
