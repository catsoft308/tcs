﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Reports - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<%: Styles.Render("~/Content/Shared/css") %>
<!DOCTYPE html>
<html ng-app="TCS" ng-controller="ReportsController">
<body>

<!-- Main Content Panel -->
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Reports</h2>
    </div>
    <div class="panel-body panel-body-main">
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h4><a ng-click="report1()" class="hoverHand" style="float:left; display:inline; padding:10px;">Daily Appointments</a></h4>
                <h4><a ng-click="report2()" class="hoverHand" style="float:left; display:inline; padding:10px;">Weekly Appointments</a></h4>
                <h4><a ng-click="report3()" class="hoverHand" style="float:left; display:inline; padding:10px;">Courses by Term</a></h4>
                <h4><a ng-click="report4()" class="hoverHand" style="float:left; display:inline; padding:10px;">Apointments by Term</a></h4>
            </div>

            <!-- Daily Appointments Report Panel -->
            <div class="panel-body panel-body-inner" ng-show="selectedReport == 1">
                <div>
                    <select ng-model="selectedTermR1" ng-options="term as term.name for term in terms" ng-change="report1()"></select>
                </div>
                <div class="panel panel-default box-shadow--2dp">
                    <div class="panel-heading">
                        <h3 class="panel-title">Student Appointments In Term Daily</h3>
                    </div>
                    <div class="panel-body panel-body-inner">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Appointments</th>
                                </tr>
                            </thead>
                            <tr ng-repeat="model in reportData" ng-hide="loading">
                                <td>
                                    {{model.date}}
                                </td>
                                <td>
                                    {{model.appointments}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body panel-body-inner" ng-show="loading">
                        Loading...
                    </div>
                </div>
            </div>

            <!-- Weekly Apointments Report Panel -->
            <div class="panel-body panel-body-inner" ng-show="selectedReport == 2">
                <div>
                    <select ng-model="selectedTermR2" ng-options="term as term.name for term in terms" ng-change="report2()"></select>
                </div>
                <div class="panel panel-default box-shadow--2dp">
                    <div class="panel-heading">
                        <h3 class="panel-title">Student Appointments in Term Weekly</h3>
                    </div>
                    <div class="panel-body panel-body-inner">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Week of</th>
                                    <th>Appointments</th>
                                </tr>
                            </thead>
                            <tr ng-repeat="model in reportData" ng-hide="loading">
                                <td>
                                    {{model.week}}
                                </td>
                                <td>
                                    {{model.appointments}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body panel-body-inner" ng-show="loading">
                        Loading...
                    </div>
                </div>
            </div>

            <!-- Courses by Term Report Panel -->
            <div class="panel-body panel-body-inner" ng-show="selectedReport == 3">
                <div>
                    <select ng-model="selectedTermR3" ng-options="term as term.name for term in terms" ng-change="report3()"></select>
                </div>
                <div class="panel panel-default box-shadow--2dp">
                    <div class="panel-heading">
                        <h3 class="panel-title">Courses Using the Test Center</h3>
                    </div>
                    <div class="panel-body panel-body-inner">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Course</th>
                                </tr>
                            </thead>
                            <tr ng-repeat="model in reportData" ng-hide="loading">
                                <td>
                                    {{model.course}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body panel-body-inner" ng-show="loading">
                        Loading...
                    </div>
                </div>
            </div>

            <!-- Appointments by Term Report Panel -->
            <div class="panel-body panel-body-inner" ng-show="selectedReport == 4">
                <div>
                    <select ng-model="selectedTermR41" ng-options="term as term.name for term in terms" ng-change="report4()"></select>
                    -
                    <select ng-model="selectedTermR42" ng-options="term as term.name for term in terms" ng-change="report4()"></select>
                </div>
                <div class="panel panel-default box-shadow--2dp">
                    <div class="panel-heading">
                        <h3 class="panel-title">Appointments over Terms</h3>
                    </div>
                    <div class="panel-body panel-body-inner">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Term</th>
                                    <th>Appointments</th>
                                </tr>
                            </thead>
                            <tr ng-repeat="model in reportData"  ng-hide="loading">
                                <td>
                                    {{model.term}}
                                </td>
                                <td>
                                    {{model.appointments}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body panel-body-inner" ng-show="loading">
                        Loading...
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
</asp:Content>
