﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Exams - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<%: Styles.Render("~/Content/Shared/css") %>
<!DOCTYPE html>
<html ng-app="TCS" ng-controller="AdminExamsController">
<body>

<!-- Main Content Panel -->
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Exams</h2>
    </div>
    <div class="panel-body panel-body-main">
        <div>
            <select ng-model="selectedTerm" ng-options="term as term.name for term in terms" ng-change="loadInfo()"></select>
        </div>

        <!-- Approved Exams Panel -->
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h3 class="panel-title">Approved Exams</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Created By</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in approvedExams track by model.id" ng-hide="loading">
                        <td>
                            {{model.courseID}}
                        </td>
                        <td>
                            {{model.name}}
                        </td>
                        <td>
                            {{model.createdBy}}
                        </td>
                        <td>
                            {{model.startTime}}
                        </td>
                        <td>
                            {{model.endTime}}
                        </td>
                        <td>
                            <button type="button" ng-click="setCurrentExam(model)" class="btn btn-danger" data-toggle="modal" data-target="#CancelExam">Cancel</button>
                        </td>
                        <td>
                            <button type="button" ng-click="seeExamInfo(model)" class="btn btn-default" data-toggle="modal" data-target="#ExamInfo">Info</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>

        <!-- Pending Exam Requests Panel -->
        <div class="panel panel-default box-shadow--2dp">
            <div class="panel-heading">
                <h3 class="panel-title">Pending Exam Requests</h3>
            </div>
            <div class="panel-body panel-body-inner">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Exam</th>
                            <th>Created By</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tr ng-repeat="model in pendingExams track by model.id" ng-hide="loading">
                        <td>
                            {{model.courseID}}
                        </td>
                        <td>
                            {{model.name}}
                        </td>
                        <td>
                            {{model.createdBy}}
                        </td>
                        <td>
                            {{model.startTime}}
                        </td>
                        <td>
                            {{model.endTime}}
                        </td>
                        <td>
                            <button type="button" ng-click="getUtilization(model)" class="btn btn-success" data-toggle="modal" data-target="#ApproveRequest">Approve</button>
                        </td>
                        <td>
                            <button type="button" ng-click="getUtilization(model)" class="btn btn-danger" data-toggle="modal" data-target="#DenyRequest">Deny</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div ng-show="loading">
                Loading...
            </div>
        </div>
    </div>

    <!-- Cancel Exam Modal -->
    <div class="modal fade" id="CancelExam" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cancel Exam</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to cancel {{currentExam.courseID + " " + currentExam.name}}?</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" ng-click="cancelExam()">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Exam Info Modal -->
    <div class="modal fade" id="ExamInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Info for {{currentExam.courseID + " " + currentExam.name}}</h4>
          </div>
          <div class="modal-body">
            <p>Exam Time: {{currentExam.startTime + " - " + currentExam.endTime}}</p>
            <p>Exam Duration: {{currentExam.duration}} minutes</p>
            <div class="panel panel-default box-shadow--2dp">
                <div class="panel-heading">
                    <h3 class="panel-title">Student Appointments</h3>
                </div>
                <div class="panel-body panel-body-inner">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Student ID</th>
                                <th>Time</th>
                                <th></th>
                                <th></th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="model in studentAppointments track by model.studentID" ng-hide="modalLoading">
                            <td>
                                {{model.studentID}}
                            </td>
                            <td>
                                {{model.timeString}}
                            </td>
                            <td>
                                <button type="button" ng-click="setCurrentAppointment(model)" class="btn btn-info" data-dismiss="modal" data-toggle="modal" data-target="#EditAppointment">Edit</button>
                            </td>
                            <td>
                                <button type="button" ng-click="setCurrentAppointment(model)" class="btn btn-danger" data-dismiss="modal" data-toggle="modal" data-target="#CancelAppointment">Cancel</button>
                            </td>
                            <td ng-hide="model.attended">
                                <button type="button" ng-click="checkInStudent(model)" class="btn btn-success">Check-In</button>
                            </td>
                            <td ng-show="model.attended">
                                <i>Attended</i>
                            </td>
                        </tr>
                    </table>
                </div>
                <div ng-show="modalLoading">
                    Loading...
                </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Cancel Appointment Modal -->
    <div class="modal fade" id="CancelAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cancel Exam</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to cancel {{currentAppointment.studentID + "'s appointment for " + currentExam.name}}?</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="modal" data-target="#ExamInfo" ng-click="cancelAppointment()">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#ExamInfo">No</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Edit Appointment Modal -->
    <div class="modal fade" id="EditAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Editing Appointment for {{currentAppointment.studentID + " on " + currentExam.name}}</h4>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td>Seat Number:</td>
                    <td>
                        <input type="text" ng-model="currentAppointment.seatNumber" />
                    </td>
                </tr>
                <tr>
                    <td>Appointment Time:</td>
                    <td>
                        <input type="date" class="form-control" uib-datepicker-popup ng-model="currentAppointment.date" is-open="" min-date="" max-date="" datepicker-options="dateOptions" ng-required="true" close-text="Close" />
                        at
                        <input ui-timepicker="timePickerOptions" ng-model="currentAppointment.time" base-date="appt.date" style="width: 60px;"></input>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal" data-toggle="modal" data-target="#ExamInfo" ng-click="editAppointment()">Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#ExamInfo">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Approve Exam Request Confirmation Modal -->
    <div class="modal fade" id="ApproveRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Approve Exam Request</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to approve the request for {{currentExam.courseID + " " + currentExam.name}}
                at {{currentExam.startTime + " - " + currentExam.endTime}}? Exam Duration: {{currentExam.duration}} minutes</p>
            <div class="panel panel-default box-shadow--2dp">
                <div class="panel-heading">
                    <h3 class="panel-title">Utilization</h3>
                </div>
                <div class="panel-body panel-body-inner">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Utilization</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="(date, util) in utilization" ng-hide="modalLoading">
                            <td>
                                {{date}}
                            </td>
                            <td>
                                {{util | number:2}}
                            </td>
                        </tr>
                    </table>
                </div>
                <div ng-show="modalLoading">
                    Loading...
                </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="approveExamRequest()">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Deny Exam Request Confirmation Modal -->
    <div class="modal fade" id="DenyRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Deny Exam Request</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to deny the request for {{currentExam.courseID + " " + currentExam.name}}
                at {{currentExam.startTime + " - " + currentExam.endTime}}? Exam Duration: {{currentExam.duration}} minutes</p>
              <div class="panel panel-default box-shadow--2dp">
                <div class="panel-heading">
                    <h3 class="panel-title">Utilization</h3>
                </div>
                <div class="panel-body panel-body-inner">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Utilization</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="(date, util) in utilization" ng-hide="modalLoading">
                            <td>
                                {{date}}
                            </td>
                            <td>
                                {{util | number:2}}
                            </td>
                        </tr>
                    </table>
                </div>
                <div ng-show="modalLoading">
                    Loading...
                </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" ng-click="denyExamRequest()">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</body>
</html>
</asp:Content>
