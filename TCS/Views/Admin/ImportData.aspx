﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Import Data - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Styles.Render("~/Content/Shared/css") %>
    <!DOCTYPE html>
    <html ng-app="TCS" ng-controller="ImportDataController">

    <!-- Main Content Panel -->
    <body>
        <div class="panel panel-main panel-default box-shadow--6dp">
            <div class="panel-heading panel-heading-main">
                <h2 class="panel-title">Import Data</h2>
            </div>
            <div class="panel-body panel-body-main">
                
                <%-- Directive for the uploader form --%>
                <eg-photo-uploader></eg-photo-uploader>

            </div>
        </div>

        <!-- Upload Confirmation Modal -->
        <div class="modal fade" id="ConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Overwrite Term Data</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to overwrite the data for {{selectedTerm.name}}?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="submit()">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </div>
    </body>
    </html>
</asp:Content>
