﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Edit Center Info - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Styles.Render("~/Content/Shared/css") %>
    <!DOCTYPE html>
    <html ng-app="TCS" ng-controller="EditCenterInfoController">
    <body>

        <!-- Main Content Panel -->
        <div class="panel panel-main panel-default box-shadow--6dp">
            <div class="panel-heading panel-heading-main">
                <h2 class="panel-title">Edit Testing Center Information</h2>
            </div>
            <div class="panel-body panel-body-main">

                <!-- Panel containing fields -->
                <div class="panel panel-default box-shadow--2dp">
                    <div class="panel-body panel-body-inner">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>

                            <!-- Num Seats -->
                            <tr>
                                <td>Number of Seats:</td>
                                <td>
                                    <input type="text" ng-model="numSeats" style="width: 30px;" />
                                    <button type="button" ng-click="submitFunction = changeNumSeats" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>

                            <!-- Num Set-Aside Seats -->
                            <tr>
                                <td>Number of Set-Aside Seats:</td>
                                <td>
                                    <input type="text" ng-model="numSetAsideSeats" style="width: 30px;" />
                                    <button type="button" ng-click="submitFunction = changeNumSetAsideSeats" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>

                            <!-- Center Hours -->
                            <tr>
                                <td>Center Hours:</td>
                                <td>
                                    <select ng-model="centerHoursTerm" ng-options="term as term.name for term in terms" ng-change="centerHoursInfo()" style="width: 120px;"></select>
                                    
                                    <span>Weekday:</span>
                                    <!-- Weekday Open -->
                                    <input ui-timepicker="timePickerOptions" ng-model="centerHours.openTimeWeekday" base-date="appt.date" style="width: 60px;"></input>
                                    -
                                    <!-- Weekday Close -->
                                    <input ui-timepicker="timePickerOptions" ng-model="centerHours.closeTimeWeekday" base-date="appt.date" style="width: 60px;"></input>
                                    
                                    <span>Weekend:</span>
                                    <!-- Weekend Open -->
                                    <input ui-timepicker="timePickerOptions" ng-model="centerHours.openTimeWeekend" base-date="appt.date" style="width: 60px;"></input>
                                    -
                                    <!-- Weekend Close -->
                                    <input ui-timepicker="timePickerOptions" ng-model="centerHours.closeTimeWeekend" base-date="appt.date" style="width: 60px;"></input>
                                    
                                    <button type="button" ng-click="submitFunction = changeCenterHours" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>

                            <!-- Available Dates -->
                            <tr>
                                <td>Available Dates:</td>
                                <td>
                                    <select ng-model="openDatesTerm" ng-options="term as term.name for term in terms" ng-change="openDatesInfo()" style="width: 120px;"></select>

                                    <!-- Start Date -->
                                    <input type="date" class="form-control" uib-datepicker-popup ng-model="openDates.startDate" is-open="" min-date="" max-date="" datepicker-options="dateOptions" ng-required="true" close-text="Close" />
                                    -
                                    <!-- End Date -->
                                    <input type="date" class="form-control" uib-datepicker-popup ng-model="openDates.endDate" is-open="" min-date="" max-date="" datepicker-options="dateOptions" ng-required="true" close-text="Close" />
                                    <button type="button" ng-click="submitFunction = changeOpenDates" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>

                            <!-- Non-SBU Reservation Dates -->
                            <tr>
                                <td>Non-SBU Reservation Dates:</td>
                                <td>
                                    <select ng-model="nonSBUDatesTerm" ng-options="term as term.name for term in terms" ng-change="nonSBUDatesInfo()" style="width: 120px;"></select>
                                    <!-- Start Date -->
                                    <input type="date" class="form-control" uib-datepicker-popup ng-model="nonSBUDates.startDate" is-open="" min-date="" max-date="" datepicker-options="dateOptions" ng-required="true" close-text="Close" />
                                    -
                                    <!-- End Date -->
                                    <input type="date" class="form-control" uib-datepicker-popup ng-model="nonSBUDates.endDate" is-open="" min-date="" max-date="" datepicker-options="dateOptions" ng-required="true" close-text="Close" />

                                    <button type="button" ng-click="submitFunction = changeNonSBUDates" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>

                            <!-- Gap Time -->
                            <tr>
                                <td>Gap Time (Minutes):</td>
                                <td>
                                    <input type="text" ng-model="gapTime" style="width: 30px;" />
                                    <button type="button" ng-click="submitFunction = changeGapTime" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>

                            <!-- Reminder Interval -->
                            <tr>
                                <td>Reminder Interval (Minutes):</td>
                                <td>
                                    <input type="text" ng-model="reminderInterval" style="width: 30px;" />
                                    <button type="button" ng-click="submitFunction = changeReminderInterval" class="btn btn-success" data-toggle="modal" data-target="#SubmitModal">Submit</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Submit Change Confirmation Modal -->
            <div class="modal fade" id="SubmitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Are you sure?</h4>
                        </div>
                        <div class="modal-body">
                            <p>????</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="submitFunction()">Yes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>

    </body>
    </html>
</asp:Content>
