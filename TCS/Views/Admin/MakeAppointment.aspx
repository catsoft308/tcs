﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="homeTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Make Appointment For A Student - Frey Hall Testing Center
</asp:Content>

<asp:Content ID="homeContent" ContentPlaceHolderID="MainContent" runat="server">
        <%: Styles.Render("~/Content/Shared/css") %>

<!DOCTYPE html>
<html ng-app="TCS" ng-controller="AdminMakeAppointmentController">
<body>

<!-- Main Content Panel-->
<div class="panel panel-main panel-default box-shadow--6dp">
    <div class="panel-heading panel-heading-main">
        <h2 class="panel-title">Make Appointment</h2>
    </div>

    <!-- Make Appointment Request Panel-->
    <div class="panel-body panel-body-main">
        <h4 style="margin-bottom: 20px" class="panel-title">Select a Date and Time you would like to take {{exam.name}} for {{exam.className.sub}} {{exam.className.catNo}}:</h4>

        <form class="form-inline">
            <div class="form-group col-md-5">
                <!-- Date picker -->
                <p class="input-group">
                    <input type="text" readonly onkeydown="event.preventDefault()" class="form-control" uib-datepicker-popup="{{format}}" show-weeks="false" ng-model="appt.time" is-open="opened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" ng-change="generateTime()" />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-click="toggleOpen()"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                </p>                   
            </div>
            <div class="form-group col-md-5">
                <strong>Choose a Time:</strong>
                <!-- Time picker -->
                <input id="timePicker" onkeydown="event.preventDefault()" ui-timepicker="timePickerOptions" ng-model="appt.time"></input>
            </div>
            <div>
                <strong>Use Set Aside Seat: </strong>
                <input type="checkbox" checked ng-model="appt.setAsideSeat" />
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-success" ng-click="requestAppointment()" data-toggle="modal" data-target="#ApptNotif">Submit</button>
            </div>
        </form>
    </div>

    <!-- Appointment Request Response Modal -->
    <div class="modal fade" id="ApptNotif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-show="complete" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">{{(appt.approved) ? "Success!" : "Denied."}}</h4>
          </div>
          <div class="modal-body">
            <p>Your Appointment for the {{exam.className.sub}} {{exam.className.catNo}} exam {{exam.name}}:</p>
              <h5>{{appt.time.toDateString()}} at {{appt.time.toTimeString()}}</h5>
            <p>has been {{(appt.approved) ? "approved" : "denied"}}.</p>
          </div>
          <div class="modal-footer">
            <button ng-show="appt.approved" onclick="location.href='Home'" type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
            <button ng-show="!appt.approved" type="button" class="btn btn-default" data-dismiss="modal" >Close</button>

          </div>
        </div>
      </div>
    </div>

</div>
</body>
</html>
</asp:Content>
