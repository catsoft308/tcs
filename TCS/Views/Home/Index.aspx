﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Test Page
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<!DOCTYPE html>
<html ng-app="TCS" ng-controller="IndexController">
<head>
    <title ng-bind="models.helloAngular"></title>
</head>
<body>
    <input type="text" ng-model="models.helloAngular" />
    <h1>{{models.helloAngular}}</h1>
</body>
</html>
</asp:Content>
