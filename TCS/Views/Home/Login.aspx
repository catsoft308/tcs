﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Login - Frey Hall Test Center
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Styles.Render("~/Content/Home/css") %>
    <!DOCTYPE html>
    <html ng-app="TCS" ng-controller="LoginController">
        <style>
            .my-module {
                position: fixed;
                top: 0;
                left: 0;
            }
        </style>
    <div class="" id="login-modal" role="dialog" aria-labelledby="myModalLabel">
        <div >
            <div class="loginmodal-container">
                <form ng-submit="login()">
                    <input ng-model="form.username" type="text" id="user" placeholder="NetID/Email">
                    <input ng-model="form.password" type="password" id="pass" placeholder="Password">
                    <input id="loginButton" type="submit" class="login loginmodal-submit" value="Login" data-toggle="modal" data-target="#multiRole" data-dismiss="login-modal">
                </form>
            </div>
        </div>
    </div>

    <!-- MultiRole Login Modal -->
    <div class="modal my-module" id="multiRole" role="dialog" aria-labelledby="myModalLabel" ng-show="respModal" aria-hidden="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Login Response</h4>
          </div>
          <div class="modal-body">
            <h4>{{loginResp}}</h>
          </div>
          <div class="modal-footer">
            <button ng-show="role[0]==1" onclick="location.href='/Admin/Home'" type="button" class="btn btn-default" data-dismiss="modal" >Admin</button>
            <button ng-show="role[1]==1" onclick="location.href='/Students/Home'" type="button" class="btn btn-default" data-dismiss="modal" >Student</button>
            <button ng-show="role[2]==1" onclick="location.href='/Professors/Home'" type="button" class="btn btn-default" data-dismiss="modal" >Professor</button>


          </div>
        </div>
      </div>
    </div>
    </html>
</asp:Content>
