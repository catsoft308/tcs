﻿using System;
using System.Linq;
using System.Web;
using TCS.App_Data;
using TCS.DAL;

namespace TCS.Services
{
    public class UserService
    {
        // User roles, which also correspond to urls (Views/...)
        public static int ADMIN_ROLE = 0;
        public static int PROFESSOR_ROLE = 2;
        public static int STUDENT_ROLE = 1;

        /// <summary>
        /// If username and password match, sign in appropriate user to the session
        /// </summary>
        /// <param name="username">The username to try</param>
        /// <param name="password">The password to try</param>
        /// <returns>The root url to redirect to (ex: "Admin" for Admin/Home)</returns>
        internal static string userLogin(string username, string password)
        {
            var db = new TCSEntities();

            var user = db.User.Find(username);
            if (user != null)
            {
                if (user.Password == password)
                {
                    string roles = "";
                    roles = Utils.getRoleString(user.Role);

                    Logger.LogInfo("User logged in: " + user.ID);

                    // If User could be signed in and assigned a role, create user in context
                    if (roles != "")
                    {
                        // Store user roles in session
                        HttpContext.Current.Session[Constants.SessionStrings.ROLES] = roles;
                        HttpContext.Current.Session[Constants.SessionStrings.USER_ID] = user.ID;
                        return roles.Split(',').First();
                    }
                }
                return "000";
            }
            else
                return "000";
        }

        /// <summary>
        /// See if the currently signed in user has the given role
        /// </summary>
        /// <param name="role">The role to check</param>
        /// <returns>If the role is assigned</returns>
        internal static bool UserHasRole(int role)
        {
            try
            {
                string roleString = (string)HttpContext.Current.Session[Constants.SessionStrings.ROLES];
                return roleString.ToCharArray()[role] == '1';
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Get an object containing info for the currently signed in user
        /// </summary>
        /// <returns>A dynamic containing user info</returns>
        internal static dynamic GetSignedInUser()
        {
            using (var db = new TCSEntities())
            {
                string userID;
                try
                {
                    userID = (string)HttpContext.Current.Session[Constants.SessionStrings.USER_ID];
                }
                catch (Exception e)
                {
                    return null;
                }
                var user = (from u in db.User
                            where u.ID == userID
                            select new {
                                netID = u.ID,
                                firstName = u.FirstName,
                                lastName = u.LastName,
                                email = u.Email
                            }).FirstOrDefault();
                return user;
            }
        }
    }
}