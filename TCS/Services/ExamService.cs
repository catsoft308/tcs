﻿using System;
using System.Collections.Generic;
using System.Linq;
using TCS.DAL;
using System.Web;
using TCS.App_Data;

namespace TCS.Services
{
    public class ExamService
    {
        public static Boolean scheduleExam(string name, int duration, DateTime startTime, DateTime endTime, int termID, string netID, int classID)
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var exam = new DAL.Exam();

                exam.DURATION = duration;
                exam.StartTime = startTime;
                exam.EndTime = endTime;
                exam.NAME = name;
                exam.TERM_ID = termID;
                exam.CreatedBy = netID;
                exam.classID = classID;
                exam.Approved = 0;

                db.Exam.Add(exam);
                int ret = 0;
                try
                {
                    ret = db.SaveChanges();
                    Logger.LogInfo(string.Format(@"Exam #{0}, ""{1}"" schedule request submitted", exam.ID, exam.NAME));
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                return ret==1;
            }
        }

        public static dynamic getUpcomingExamsByUser(string netID)
        {
            var db2 = new TCSEntities();
            using (var db = new TCSEntities())
            {
                string[] n = { netID };
                var classIdsRaw = db.Database.SqlQuery(typeof(int), "Select r.class_id from roster r where r.netid = ?", n);

                List<int> classIds = new List<int>();
                foreach(int id in classIdsRaw)
                {
                    classIds.Add(id);
                }

                var apptsAlreadyRaw = db.Database.SqlQuery(typeof(int), "select a.exam_id from appointment a where a.appointment_owner = ? ", n);

                List<int> apptsAlready = new List<int>();
                foreach (int id in apptsAlreadyRaw)
                {
                    apptsAlready.Add(id);
                }

                var nowPlus30 = DateTime.Now.AddMinutes(30);
                var exams = from e in db.Exam
                            where classIds.Contains((int)e.classID) &&
                            !apptsAlready.Contains((int)e.ID)
                            && e.EndTime > nowPlus30
                            && e.Approved == 1
                            select new { e.NAME, e.classID, e.StartTime, e.EndTime, e.ID};
                var ex = exams.AsEnumerable()
                    .Select(e => new
                    {
                        name = e.NAME,
                        className = db2.Class.Where(c => c.ID == e.classID).Select(m => new { sub = m.Subject, catNo = m.CatalogID }).FirstOrDefault(),
                        start = e.StartTime.ToString(),
                        end = e.EndTime.ToString(),
                        id = e.ID
                    });
                    
                    return (ex==null)?null:ex.ToList();

            }
        }

        /// <summary>
        /// Returns an enumerable of exams whose range overlap the given date
        /// </summary>
        public static List<Exam> ExamsOverlappingDate(DateTime date)
        {
            using (var db = Database.ContextReadOnly)
            {
                return db.Exam.Where(e => e.Approved == 1 && (date > e.StartTime && date < e.EndTime)).ToList();
            }
        }

        /// <summary>
        /// Returns an enumerable of users who have yet to take the exam
        /// </summary>
        public static List<User> StudentsWhoNeedToTake(Exam exam)
        {
            using (var db = Database.ContextReadOnly)
            {
                // get users in course
                return db.User.Where(u => u.Classes.Any(c => c.ID == exam.classID)).ToList();
            }
        }

        /// <summary>
        /// Returns the number of days in the range of the exam schedule
        /// </summary>
        public static double DaysInExam(Exam exam)
        {
            return (exam.EndTime - exam.StartTime).Value.TotalDays;
        }

        /// <summary>
        /// Load an individual exam by ID
        /// </summary>
        public static Exam Load(int examId)
        {
            using (var db = Database.ContextReadOnly)
            {
                return db.Exam.SingleOrDefault(e => e.ID == examId);
            }
        }
    }
}