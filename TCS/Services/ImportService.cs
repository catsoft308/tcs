﻿using System;
using System.Linq;
using TCS.App_Data;
using TCS.CSVImport;
using TCS.DAL;

namespace TCS.Services
{
    public class ImportService
    {
        #region Individual Add Methods
        /// <summary>
        /// Given an array of strings representing a line of a csv, try to create a User account for the fields
        /// Fields are given as { FirstName, LastName, NetID, Email }
        /// </summary>
        private static void TryAddUser(string[] fields, Enums.Role role)
        {
            using (var db = Database.Context)
            {
                var netId = fields[2];
                var user = db.User.SingleOrDefault(u => u.ID == netId);
                if (user == null)
                {
                    user = new User();
                    db.User.Add(user);
                    user.ID = netId;
                }

                user.FirstName = fields[0];
                user.LastName = fields[1];
                user.Email = fields[3];
                user.Password = Utils.InitialPassword(netId);
                user.Role = (sbyte)role;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                finally
                {
                    db.Dispose();
                }
            }
        }

        /// <summary>
        /// Given an array of strings representing a line of a csv, try to create a Roster association
        /// Fields are given as { NetID, ClassID }
        /// </summary>
        private static void TryAddRosterEntry(string[] fields)
        {
            using (var db = Database.Context)
            {
                var netId = fields[0];
                var user = db.User.SingleOrDefault(u => u.ID == netId);
                var classId = int.Parse(fields[1].Split('-')[0]);   // other part is termID
                var theClass = db.Class.SingleOrDefault(c => c.ID == classId);

                // just return if null user
                if (user == null)
                {
                    Logger.LogWarning(string.Format("No user with ID={0}. Skipping.", fields[0]));
                    return;
                }

                // is the user already in this class?
                var userClass = user.Classes.SingleOrDefault(uc => uc.ID == classId);
                if (userClass != null || theClass == null)
                {
                    Logger.LogWarning(string.Format("User {0} is already in class {1}. Skipping.", fields[0], classId));
                    return;
                }

                theClass.Students.Add(user);
                user.Classes.Add(theClass);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                db.Dispose();
            }
        }

        /// <summary>
        /// Given an array of strings representing a line of a csv, try to create a Class record
        /// Fields are given as { ClassID, Subject, CatalogNumber, Section, InstructorNetID }
        /// </summary>
        private static void TryAddClass(string[] fields)
        {
            if (Utils.AnyNullOrEmpty(fields))
            {
                // ideally, tell to user
                Logger.LogWarning("Null or empty fields; skipping");
            }
            using (var db = Database.Context)
            {
                var classId = int.Parse(fields[0].Split('-')[0]);
                var termId = int.Parse(fields[0].Split('-')[1]);

                // Get the class from DB, or create a new Class if one isn't found
                var theClass = db.Class.SingleOrDefault(c => c.ID == classId);
                if (theClass == null)
                {
                    theClass = new Class();
                    db.Class.Add(theClass);
                    theClass.ID = classId;
                }

                // make sure teacher is valid (FK constraint)
                var instrID = fields[4];
                var instructor = db.User.SingleOrDefault(u => u.ID == instrID);
                if (instructor == null || !Utils.IsInstructor(instructor.Role))
                {
                    Logger.LogWarning("There is no instructor user with ID=`" + instrID + "`; skipping adding class `" + classId + "`");
                    return;
                }

                // edit fields
                theClass.TERM_ID = termId;
                theClass.InstructorID = fields[4];
                theClass.CatalogID = fields[2];
                theClass.Section = fields[3];
                theClass.Subject = fields[1];

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }
                finally
                {
                    db.Dispose();
                }
            }
        }
        #endregion


        #region Public Import Methods
        /// <summary>
        /// Given the path to a users csv, parse and add new users as students
        /// </summary>
        public static bool ImportStudents(string path)
        {
            return CsvParser.ParseCsv(path, c => TryAddUser(c, Enums.Role.Student), 1);
        }

        /// <summary>
        /// Given the path to a users csv, parse and add new users as instructors
        /// </summary>
        public static bool ImportInstructors(string path)
        {
            return CsvParser.ParseCsv(path, c => TryAddUser(c, Enums.Role.Instructor), 1);
        }

        /// <summary>
        /// Given path to a classes csv, add class records
        /// </summary>
        public static bool ImportClasses(string path)
        {
            return CsvParser.ParseCsv(path, TryAddClass, 1);
        }

        /// <summary>
        /// Given path to a roster csv, add user-class associations
        /// </summary>
        public static bool ImportRoster(string path)
        {
            return CsvParser.ParseCsv(path, TryAddRosterEntry, 1);
        }
        #endregion
    }
}