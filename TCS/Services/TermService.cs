﻿using System.Collections.Generic;
using System.Linq;
using TCS.DAL;

namespace TCS.Services
{
    public class TermService
    {
        /// <summary>
        /// Get all the terms in the database
        /// </summary>
        public static List<Term> GetTerms()
        {
            using (var db = Database.ContextReadOnly)
            {
                return db.Term.ToList();
            }
        }

        /// <summary>
        /// Load a specific term by ID
        /// </summary>
        public static Term Load(int termId)
        {
            using (var db = Database.ContextReadOnly)
            {
                return db.Term.SingleOrDefault(t => t.ID == termId);
            }
        }
    }
}