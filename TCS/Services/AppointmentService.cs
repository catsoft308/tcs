﻿using System;
using System.Collections.Generic;
using System.Linq;
using TCS.DAL;

namespace TCS.Services
{
    public class AppointmentService
    {
        public static dynamic getUpcomingApptsByUser(string netID)
        {
            var db2 = new TCSEntities();
            using (var db = new TCSEntities())
            {
                var appts = db.Appointment
                            .Where(a => a.OwnerID == netID)
                            .Where(a => a.APPT_TIME > DateTime.Now)
                            .Select(p => new {
                                p.Exam.NAME,
                                p.Exam.classID,
                                p.APPT_TIME,
                                p.ID,
                                p.AssignedSeat
                            })
                            .AsEnumerable().Select(p => new
                            {
                                id = p.ID,
                                seatNumber = p.AssignedSeat,
                                examName = p.NAME,
                                classID = p.classID,
                                apptTime = p.APPT_TIME.ToString(),
                                apptDateTime = p.APPT_TIME,
                                className = db2.Class.Where(c => c.ID == p.classID).Select(m => new { sub = m.Subject, catNo = m.CatalogID }).FirstOrDefault(),
                                status = "upcoming",
                            });
                return (appts==null)?null:appts.ToList();
            }
        }
        public static dynamic getPreviousApptsByUser(string netID)
        {
            var db2 = new TCSEntities();
            using (var db = new TCSEntities())
            {
                var appts = db.Appointment
                            .Where(a => a.OwnerID == netID)
                            .Where(a => a.APPT_TIME < DateTime.Now)
                            .Select(p => new {
                                p.Exam.NAME,
                                p.Exam.classID,
                                p.APPT_TIME,
                                p.ID,
                                p.AssignedSeat
                            })
                            .AsEnumerable().Select(p => new
                            {
                                id = p.ID,
                                seatNumber = p.AssignedSeat,
                                examName = p.NAME,
                                classID = p.classID,
                                apptTime = p.APPT_TIME.ToString(),
                                className = db2.Class.Where(c => c.ID == p.classID).Select(m => new { sub = m.Subject, catNo = m.CatalogID }).FirstOrDefault(),
                                status = "completed"
                            });

                return (appts==null)?null:appts.ToList();
            }
        }

        /// <summary>
        /// returns whether the appointment was added or not
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool requestAppointment(DateTime apptTime, int examID, string createdBy, string apptOwner, bool setAside)
        {
            bool schedulable = true;
            List<int>[] apptsChairs = new List<int>[2];
            apptsChairs[0] = new List<int>();
            apptsChairs[1] = new List<int>();
            using (var db = new TCSEntities())
            {
                var appt = new Appointment
                {
                    APPT_TIME = apptTime,
                    CreatedBy = createdBy,
                    OwnerID = apptOwner,
                    ExamID = examID,
                    CheckedIn = 0,
                    IsSuperfluous = 0
                };

                Logger.LogInfo(string.Format(@"User '{0}' is requesting an appointment for student={1}, examID={2}, dateTime={3}", createdBy, apptOwner, examID, apptTime));

                var examDur = db.Exam.First(e => e.ID == appt.ExamID).DURATION;
                var examDurStatic = db.Exam.First(e => e.ID == appt.ExamID).DURATION;

                //check if there aren't too many other appointments during the duration of the appointment being validated
                var nextDate = apptTime;
                while(examDur >= 0)
                {
                    List<int>[] curApptsChairs = getApptsAtTime(appt.ExamID, nextDate, db);
                    int apptsAtTime = curApptsChairs[0].Count + curApptsChairs[1].Count;
                    if (apptsAtTime >= db.SystemValues.Single().NBR_SEATS - db.SystemValues.Single().SET_ASIDE_SEATS)
                    {
                        Logger.LogInfo("Appointment is not schedulable; no seats available at this time.");
                        schedulable = false;
                    }
                    else
                    {
                        foreach (int chair in curApptsChairs[0].AsEnumerable())
                        {
                            apptsChairs[0].Add(chair);
                        }
                        foreach (int chair in curApptsChairs[1].AsEnumerable())
                        {
                            apptsChairs[1].Add(chair);
                        }
                    }

                    examDur -= 30;
                    nextDate = nextDate.AddMinutes(30);
                }

                if (schedulable == true)
                {
                    //choose assigned seat
                    bool goodSeat = false;
                    foreach(int seat in apptsChairs[0])
                    {
                        int nextAvailSeat = seat + 2;
                        if (nextAvailSeat <= db.SystemValues.Single().NBR_SEATS)
                        {
                            if (!apptsChairs[0].Contains(nextAvailSeat) && !apptsChairs[1].Contains(nextAvailSeat))
                            {
                                appt.AssignedSeat = nextAvailSeat;
                                goodSeat = true;
                            }
                        }
                    }
                    //if we couldn't find a seat with at least one space between the other appointments for this exam,
                    //stick it in the first available seat sequentially.
                    if(goodSeat == false)
                    {
                        int trySeat = 1;
                        while(goodSeat == false)
                        {
                            if(!apptsChairs[0].Contains(trySeat) && !apptsChairs[1].Contains(trySeat))
                            {
                                appt.AssignedSeat = trySeat;
                                goodSeat = true;
                            }
                            trySeat++;
                        }
                    }

                    //check to see if we have another appointment that conflicts with the one we're requesting
                    var upcoming = getUpcomingApptsByUser(apptOwner);
                    foreach(dynamic a in upcoming)
                    {
                        if(((DateTime)appt.APPT_TIME) <= a.apptDateTime && ((DateTime)appt.APPT_TIME).AddMinutes((int)examDurStatic) >= a.apptDateTime)
                        {
                            schedulable = false;
                        }
                    }
                    if (schedulable)
                    {
                        db.Appointment.Add(appt);
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                            throw;
                        }
                    }

                    var examName = (from e in db.Exam
                                where e.ID == examID
                                select e.NAME).First();

                    var email = (from u in db.User
                                 where u.ID == apptOwner
                                 select u.Email).First();

                    try
                    {
                        Logger.LogInfo("Sending confirmation email to " + email);
                        EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, email,
                            "Frey Hall Exam Appointment",
                            "<p>You now have an appointment to take " + examName + " at " + appt.APPT_TIME.ToString() +
                            ", seat number " + appt.AssignedSeat + " in the Frey Hall Testing Center.</p>");
                    }
                    catch (Exception e)
                    {
                        Logger.LogError(e.GetBaseException().Message);
                        throw;
                    }
                }
                
                return schedulable;
            }
        }

        //Helper Function for appointment creation. returns the seat numbers of appointments that are taking place 
        // during the nextDate argument, seperated by whether that appointment is for the current appointment's exam.
        private static List<int>[] getApptsAtTime(int examID, DateTime nextDate, TCSEntities db)
        {

            List<int>[] appsAtTime = new List<int>[2];
            appsAtTime[0] = new List<int>();
            appsAtTime[1] = new List<int>();

            var apptsOnDay = db.Appointment
                                .Select(p => new {
                                    p.APPT_TIME,
                                    p.Exam.DURATION,
                                    p.Exam.ID,
                                    p.AssignedSeat
                                })
                                .AsEnumerable().Select(p => new
                                {
                                    duration = p.DURATION,
                                    apptTime = p.APPT_TIME,
                                    examId = p.ID,
                                    seat = p.AssignedSeat
                                });
            foreach(var appt in apptsOnDay)
            {
                DateTime currentApptTime = ((DateTime)(appt.apptTime));
                DateTime currentApptTimePlusDuration = ((DateTime)(appt.apptTime)).AddMinutes(((int)(appt.duration)));

                if (currentApptTime <= nextDate && currentApptTimePlusDuration >= nextDate)
                {
                    if (appt.examId == examID)
                        appsAtTime[0].Add(((int)(appt.seat)));
                    else
                        appsAtTime[1].Add(((int)(appt.seat)));
                }
            }
            return appsAtTime;
        }

        public static int getNumApptsAvailAtTime(DateTime nextDate, TCS.DAL.TCSEntities db)
        {

            int appsAtTime = 0;

            var apptsOnDay = db.Appointment
                                .Select(p => new {
                                    p.APPT_TIME,
                                    p.Exam.DURATION,
                                    p.Exam.ID,
                                    p.AssignedSeat
                                })
                                .AsEnumerable()
                                .Where(p => p.APPT_TIME.Value.Date == nextDate.Date)
                                .Select(p => new
                                {
                                    duration = p.DURATION,
                                    apptTime = p.APPT_TIME,
                                    examId = p.ID,
                                    seat = p.AssignedSeat
                                });
            foreach (var appt in apptsOnDay)
            {
                DateTime currentApptTime = ((DateTime)(appt.apptTime));
                DateTime currentApptTimePlusDuration = ((DateTime)(appt.apptTime)).AddMinutes(((int)(appt.duration)));

                if (currentApptTime <= nextDate && currentApptTimePlusDuration >= nextDate)
                {
                    appsAtTime++;
                }
            }
            return  ((int)db.SystemValues.Single().NBR_SEATS) - ((int)db.SystemValues.Single().SET_ASIDE_SEATS)-appsAtTime;
        }

        public static bool cancelAppointment(int apptID)
        {
            using (var db = new TCSEntities())
            {
                var appt = db.Appointment.Find(apptID);

                var examName = (from e in db.Exam
                                where e.ID == appt.ExamID
                                select e.NAME).First();

                var email = (from u in db.User
                             where u.ID == appt.OwnerID
                             select u.Email).First();

                db.Appointment.Remove(appt);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.LogError(string.Format("SQL Exception: {0}", ex.GetBaseException().Message));
                    throw;
                }

                EmailService.SendEmail(EmailService.SAMPLE_EMAIL, EmailService.SAMPLE_EMAIL_PASSWORD, email,
                    "Frey Hall Exam Appointment Cancelled",
                    "<p>Your appointment for " + examName + " at " + appt.APPT_TIME.ToString() + " in the Frey Hall Testing Center has been cancelled.</p>");

                return true;
            }
        }

        /// <summary>
        /// Get all the appointments that are on this date
        /// </summary>
        public static List<Appointment> AppointmentsOnDate(DateTime date)
        {
            using (var db = Database.ContextReadOnly)
            {
                var aDate = date.AddDays(-2);
                var bDate = date.AddDays(2);
                var appts = db.Appointment.Where(a => a.APPT_TIME > aDate && a.APPT_TIME < bDate).ToList();
                return appts.Where(a => a.APPT_TIME.Value.Date == date.Date).ToList();
            }
        }

        /// <summary>
        /// Return the appointments for exam with given examId
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        public static List<Appointment> AppointmentsForExam(int examId)
        {
            using (var db = Database.ContextReadOnly)
            {
                return db.Appointment.Where(a => a.ExamID == examId).ToList();
            }
        }
    }
}