﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;

namespace TCS.Services
{
    public class Logger
    {
        //    private static Logger _instance;
        //    private string _path;

        //    public static Logger Instance
        //    {
        //        get { return _instance ?? (_instance = new Logger()); }
        //    }

        //    public Logger()
        //    {
        //        _path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "log", "log.txt");
        //    }

        /// <summary>
        /// Appends a line to the end of the log file
        /// </summary>
        private static void Append(string lvl, string msg)
        {
            //var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "log", "log.txt");
            var path = HttpContext.Current.Server.MapPath("~/App_Data/log/log.txt");
            
            var sw = new StreamWriter(path, true);

            var f = new StackTrace(2);
            var methodName = f.GetFrame(0).GetMethod().Name;

            // line to append
            var s = string.Format("{0} [{1}]: ({2}) {3}", DateTime.Now, lvl, methodName, msg);
            sw.WriteLine(s);
            sw.Close();
        }

        public static void LogError(string msg)
        {
            Append("ERROR", msg);
        }

        public static void LogInfo(string msg)
        {
            Append("INFO", msg);
        }

        public static void LogWarning(string msg)
        {
            Append("WARNING", msg);
        }
    }
}