﻿using System.Linq;
using TCS.DAL;

namespace TCS.Services
{
    public class SystemValuesService
    {
        /// <summary>
        /// Number of seats in the testing center
        /// </summary>
        public static int Seats
        {
            get
            {
                using (var db = Database.ContextReadOnly)
                {
                    return db.SystemValues.First().NBR_SEATS;
                }
            }
        }

        public static int GapTime
        {
            get
            {
                using (var db = Database.ContextReadOnly)
                {
                    return db.SystemValues.First().GAP_TIME.Value;
                }
            }
        }
    }
}