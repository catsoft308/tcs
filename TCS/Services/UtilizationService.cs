﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TCS.App_Data;
using TCS.DAL;

namespace TCS.Services
{
    public class UtilizationService
    {
        /// <summary>
        /// Returns the actual utilization for a given date.
        /// Given by:
        ///     Utilact(d) = sum(a in appts(d) => a.duration + gaptime)/(nSeats * openTime)
        /// </summary>
        public static double ActualUtilization(Term term, DateTime date)
        {
            var result = 0.0;
            var seats = SystemValuesService.Seats;
            var openTime = TimeOpen(term, date);
            var appointments = AppointmentService.AppointmentsOnDate(date.Date);
            var gapTime = SystemValuesService.GapTime;

            foreach (var a in appointments)
            {
                var exam = ExamService.Load(a.ExamID);
                var d = (exam.DURATION + gapTime + 30) / (seats * openTime);
                if (d != null) result += d.Value;
            }

            Logger.LogInfo(string.Format(@"Actual Utilization for {0}: {1}", date.ToString("MM/dd/yy"), result));
            return result;
        }

        /// <summary>
        /// Returns the expected utilization for a given date
        /// Given by:
        ///     Utilexp(d) = Utilact(d) + sum(e in exams(d) => (e.duration + gaptime) * (studentNeedToTake(e) - numberOfApptsToTake(e) / numberOfDays(e))
        /// </summary>
        public static double ExpectedUtilization(Term term, DateTime date)
        {
            var actualUtilization = ActualUtilization(term, date);
            var result = 0.0; // start with actual
            var exams = ExamService.ExamsOverlappingDate(date);
            var gapTime = SystemValuesService.GapTime;
            var seats = SystemValuesService.Seats;
            var openTime = TimeOpen(term, date);


            foreach (var e in exams)
            {
                var students = ExamService.StudentsWhoNeedToTake(e).Count();
                var appts = AppointmentService.AppointmentsForExam(e.ID).Count();
                var days = ExamService.DaysInExam(e);

                var d = (e.DURATION + gapTime) * ((students - appts) / days);
                if (d != null)
                    result += d.Value;
            }
            result /= (seats * openTime);
            result += actualUtilization;

            Logger.LogInfo(string.Format(@"Expected Utilization for {0}: {1}", date.ToString("MM/dd/yy"), result));
            return result;
        }

        /// <summary>
        /// Return a date-utilization mapping for each date within the date range
        /// </summary>
        public static Dictionary<string, double> UtilizationsForDateRange(int termId, DateTime startDate, DateTime endDate)
        {
            var map = new Dictionary<string, double>();
            var term = TermService.Load(termId);

            var startTime = DateTime.Now;
            Logger.LogInfo("Starting Utilization for range " + startDate.ToShortDateString() + " to " + endDate.ToShortDateString());

            // foreach day in the range
            for (var date = startDate.Date; date <= endDate.Date; date = date.AddDays(1))
            {
                // determine which utilization to use
                var util = (date <= DateTime.Today) ? ActualUtilization(term, date) : ExpectedUtilization(term, date);
                map.Add(date.ToString("MM/dd/yyyy"), util);
            }

            var elapsedTime = (DateTime.Now - startTime).TotalSeconds;
            Logger.LogInfo("Completed Utilization for range after " + elapsedTime + " seconds");

            return map;
        }

        /// <summary>
        /// Get the number of minutes that the center is open on a given day in this term
        /// </summary>
        private static double TimeOpen(Term term, DateTime d)
        {
            var openTime = d.IsWeekday() ? term.WKDY_OPEN : term.WKND_OPEN;
            var closeTime = d.IsWeekday() ? term.WKDY_CLOSE : term.WKND_CLOSE;

            Debug.Assert(closeTime != null, "closeTime != null");
            Debug.Assert(openTime != null, "openTime != null");

            return closeTime.Value.TotalMinutes - openTime.Value.TotalMinutes;
        }
    }
}