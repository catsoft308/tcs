﻿using System.Linq;
using TCS.DAL;

namespace TCS.Services
{
    public class ClassService
    {
        public static dynamic getClassesByTerm(int termID, string netID)
        {
            using (var db = new TCS.DAL.TCSEntities())
            {
                var classes = from c in db.Class
                              where c.TERM_ID == termID
                              && c.InstructorID == netID
                              select new { c.ID, c.Subject, c.CatalogID, c.Section };
                var classesReturn = classes.AsEnumerable().Select(c => new
                {
                    name = c.Subject + " " + c.CatalogID + "(" + c.Section + ")",
                    id = c.ID,
                });

                return (classesReturn == null) ? null : classesReturn.ToList();
            }
        }
        public static Class Load(int classId)
        {
            using (var db = Database.ContextReadOnly)
            {
                return db.Class.SingleOrDefault(c => c.ID == classId);
            }
        }
    }
}