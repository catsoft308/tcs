﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TCS.App_Data;
using TCS.Models;
using TCS.Services;

namespace TCS.CSVImport
{
    public class LocalCsvManager : ICsvImportManager
    {

        private string WorkingFolder { get; set; }

        public LocalCsvManager()
        {

        }

        public LocalCsvManager(string workingFolder)
        {
            WorkingFolder = workingFolder;
            CheckTargetDirectory();
        }

        public async Task<IEnumerable<CsvViewModel>> UploadAndImport(HttpRequestMessage request)
        {
            var provider = new CsvMultipartFormDataStreamProvider(WorkingFolder);
            await request.Content.ReadAsMultipartAsync(provider);

            var csvs = new List<CsvViewModel>();

            // get the file
            foreach (var file in provider.FileData)
            {
                var fileInfo = new FileInfo(file.LocalFileName);
                string path = fileInfo.DirectoryName + '\\' + fileInfo.Name;

                csvs.Add(new CsvViewModel
                {
                    Name = fileInfo.Name,
                    Created = fileInfo.CreationTime,
                    Modified = fileInfo.LastWriteTime,
                    Size = fileInfo.Length / 1024
                });

                // get the type
                var importType = provider.FormData["importType"];
                if (importType == null)
                {
                    // TODO error here since no type
                    Logger.LogError("No import type found.");
                    return csvs;
                }

                switch (importType)
                {
                    case Constants.ImportMethods.STUDENTS:
                        ImportService.ImportStudents(path);
                        break;
                    case Constants.ImportMethods.INSTRUCTORS:
                        ImportService.ImportInstructors(path);
                        break;
                    case Constants.ImportMethods.CLASSES:
                        ImportService.ImportClasses(path);
                        break;
                    case Constants.ImportMethods.ROSTER:
                        ImportService.ImportRoster(path);
                        break;
                }
            }

            return csvs;
        }

        public bool FileExists(string fileName)
        {
            var file = Directory.GetFiles(WorkingFolder, fileName)
                                .FirstOrDefault();

            return file != null;
        }

        private void CheckTargetDirectory()
        {
            if (!Directory.Exists(WorkingFolder))
            {
                throw new ArgumentException("the destination path " + WorkingFolder + " could not be found");
            }
        }
    }
}