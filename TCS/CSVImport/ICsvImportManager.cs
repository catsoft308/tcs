﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TCS.Models;

namespace TCS.CSVImport
{
    public interface ICsvImportManager
    {
        Task<IEnumerable<CsvViewModel>> UploadAndImport(HttpRequestMessage request);
        bool FileExists(string fileName);
    }
}
