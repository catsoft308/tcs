﻿using System;
using Microsoft.VisualBasic.FileIO;

namespace TCS.CSVImport
{
    public class CsvParser
    {
        /// <summary>
        /// Reads csv file given by path and processes them given the code provided
        /// </summary>
        public static bool ParseCsv(string path, Action<string[]> code, int skipLines = 0)
        {
            using (var parser = new TextFieldParser(path) { TextFieldType = FieldType.Delimited })
            {
                parser.SetDelimiters(",");

                while (!parser.EndOfData)
                {
                    // Skip any lines
                    while (skipLines > 0)
                    {
                        parser.ReadLine();
                        skipLines--;
                    }

                    // Process row
                    if (code == null) continue;
                    code(parser.ReadFields());
                }
                parser.Close();
                return true;
            }
        }
    }
}