﻿using System.Web;
using System.Web.Optimization;

namespace TCS
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery/jquery-ui-{version}.js",
                        "~/Scripts/jquery/jquery.timepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.unobtrusive*",
                        "~/Scripts/jquery/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap/bootstrap.js",
                        "~/Scripts/bootstrap/ui-bootstrap-tpls-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular/angular.js",
                        "~/Scripts/angular/angular-resource.js",
                        "~/Scripts/angular/angular-route.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Shared/misc/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/bootstrap.css",
                    "~/Content/bootstrap-theme.css",
                    "~/Content/bootstrap-responsive.css",
                    "~/Content/TCS.css"));

            bundles.Add(new StyleBundle("~/Content/Home/css").Include("~/Content/Home/Login.css"));
            bundles.Add(new StyleBundle("~/Content/Student/css").Include("~/Content/Student/Student.css"));
            bundles.Add(new StyleBundle("~/Content/Shared/css").Include("~/Content/Shared/Shared.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").IncludeDirectory(
                        "~/Content/themes/base", "*.css"));

            bundles.Add(new ScriptBundle("~/bundles/TCS")
                .IncludeDirectory("~/Scripts/Controllers/Admin", "*.js")
                .IncludeDirectory("~/Scripts/Controllers/Home", "*.js")
                .IncludeDirectory("~/Scripts/Controllers/Professor", "*.js")
                .IncludeDirectory("~/Scripts/Controllers/Student", "*.js")
                .IncludeDirectory("~/Scripts/Directives/", "*.js")
                .IncludeDirectory("~/Scripts/Services/", "*.js")
                .Include("~/Scripts/import/appInfo.js",
                    "~/Scripts/import/photos.js",
                    "~/Scripts/import/photoManager.js",
                    "~/Scripts/import/egAddPhoto.js",
                    "~/Scripts/import/egFiles.js",
                    "~/Scripts/import/egUpload.js",
                    "~/Scripts/import/egPhotoUploader.js",
                    "~/Scripts/import/photoManagerClient.js")
                .Include("~/Scripts/TCS.js"));
        }
    }
}