﻿var egPhotoUploader = function (appInfo, photoManager) {

    var directive = {
        link: link,
        restrict: 'E',
        templateUrl: '../Scripts/import/egPhotoUploader.html',
        scope: true
    };
    return directive;

    function link(scope, element, attrs) {
        scope.hasFiles = false;
        scope.photos = [];
        scope.upload = photoManager.upload;
        scope.appStatus = appInfo.status;
        scope.photoManagerStatus = photoManager.status;
    }
}

egPhotoUploader.$inject = ['appInfo', 'photoManager'];