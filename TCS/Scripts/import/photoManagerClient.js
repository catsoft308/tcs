﻿var photoManagerClient = function ($resource) {
    return $resource("../api/Import/:fileName",
            { id: "@fileName" },
            {
                'save': { method: 'POST', transformRequest: angular.identity, headers: { 'Content-Type': undefined } }
            });
}

photoManagerClient.$inject = ['$resource'];