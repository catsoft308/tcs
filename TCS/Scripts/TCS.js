﻿var Importer = angular.module('TCS.importer', []);
Importer.controller('photos', photos);
Importer.factory('photoManager', photoManager);
Importer.factory('photoManagerClient', photoManagerClient);
Importer.directive('egFiles', egFiles);
Importer.directive('egPhotoUploader', egPhotoUploader);
Importer.directive('egUpload', egUpload);

var TCS = angular.module('TCS', ['ngResource', 'ngRoute', 'ui.bootstrap.datepicker', 'ui.timepicker', 'TCS.importer']);
/* Controllers */
TCS.controller('IndexController', IndexController);
TCS.controller('LoginController', LoginController);
TCS.controller('StudentHomeController', StudentHomeController);
TCS.controller('MakeAppointmentController', MakeAppointmentController);
TCS.controller('AdminHomeController', AdminHomeController);
TCS.controller('AdminExamsController', AdminExamsController);
TCS.controller('UtilizationController', UtilizationController);
TCS.controller('EditCenterInfoController', EditCenterInfoController);
TCS.controller('ImportDataController', ImportDataController);
TCS.controller('ReportsController', ReportsController);
TCS.controller('ScheduleExamForStudentController', ScheduleExamForStudentController);
TCS.controller('ProfessorHomeController', ProfessorHomeController);
TCS.controller('ScheduleExamController', ScheduleExamController);
TCS.controller('AdminMakeAppointmentController', AdminMakeAppointmentController);
/* Services */
TCS.factory('tcs', tcs);
TCS.factory('appInfo', appInfo);
/* Directives */


var configFunction = function ($routeProvider, $locationProvider) {
    /**$routeProvider.
        when('/students', {
            templateUrl: 'Students/Home',
            controller: StudentHomeController
        })
        .when('/professors', {
            templateUrl: 'Professors/Home',
            controller: ProfessorHomeController
        })

        .when('/admin', {
            templateUrl: 'Admin/home',
            controller: AdminHomeController
        })
        .when('/login?returnUrl', {
            templateUrl: 'Home/Login',
            controller: LoginController
        });**/

    //$locationProvider.html5Mode(true);
}
configFunction.$inject = ['$routeProvider', '$locationProvider'];

TCS.config(configFunction);