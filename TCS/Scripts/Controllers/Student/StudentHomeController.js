﻿var StudentHomeController = function ($scope, $http, $timeout) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.terms = [];
    $scope.loading = false;
    $scope.exams = [];
    $scope.upcomingAppts = [];
    $scope.currentAppt = {};
    $scope.prevAppts = [];
    $scope.showCancelModal = false;


    //------ Scope Functions ------
    $scope.setCurrentAppt = function (appt) {
        $scope.currentAppt = appt;
    }

    $scope.makeAppointment = function (exam) {
        window.location = 'MakeAppointment?examID=' + exam;
    }

    $scope.cancelAppointment = function (classId) {
        $http.post("CancelAppointment", { apptID: $scope.currentAppt.id }).then(function (result) {
            $scope.load();
        });
    }


    //------ Initial Calls ------
    $scope.load = function()
    {
        $http.post("GetBasicInfo", {}).then(function (result) {
            $scope.loading = true;
            if (result != null) {
                $scope.terms = result.data.terms;
                $scope.user = result.data.user;
                $http.post("loadHomeData").then(function (result2) {
                    if (result2 != null) {
                        $scope.exams = result2.data.exams;
                        $scope.upcomingAppts = result2.data.upcoming;
                        $scope.prevAppts = result2.data.previous;
                    } else {
                        console.log("Error loading data.");
                    }
                });
            } else {
                console.log("Error getting info.");
            }
            $scope.loading = false;
        });
    }
    $scope.load();
}