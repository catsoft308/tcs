﻿var ProfessorHomeController = function ($scope, $timeout, $http) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.showCancelModal = false;
    $scope.loading = false;
    $scope.exams = [];
    $scope.terms = [];
    $scope.currentTerm = {};
    $scope.currentExam = {};
    $scope.studentAppointments = [];
  


    //------ Scope Functions

    $scope.loadInfo = function () {
        $scope.loading = true;
        $http.post("GetExams", { termID: $scope.currentTerm.id }).then(function (result) {
            $scope.loading = false;
            if (result.data != null) {
                $scope.exams = result.data;
            }
            else {

            }
        });
    }

    $scope.setCurrentExam = function (exam) {
        $scope.currentExam = exam;
    }

    

    $scope.seeExamInfo = function (exam) {
        $scope.currentExam = exam;
        $scope.modalLoading = true;

        $http.post("GetExamInfo", { examID: exam.id }).then(function (result) {
            $scope.modalLoading = false;

            if (result != null) {
                $scope.studentAppointments = result.data;
            } else {

            }
        });
    }


    $scope.cancelExam = function () {
        $scope.modalLoading = true;

        $http.post("CancelExam", { examID: $scope.currentExam.id }).then(function (result) {
            $scope.modalLoading = false;

            alert(result.data);
            $scope.loadInfo();
            
        });
    }



    $scope.scheduleExam = function() {

        window.location = 'ScheduleExam';
    }

    $scope.scheduleAdHocExam = function () {
        window.location = 'ScheduleAdHocExam';
    }

    //------ Initial Calls ------
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result.data != null) {
            $scope.user = result.data.user;
            $scope.terms = result.data.terms;
            $scope.currentTerm = $scope.terms[0];

            $scope.loadInfo();
        }
        else {

        }
    });

}