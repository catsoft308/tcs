﻿var ScheduleExamController = function ($scope, $http, $timeout) {
    //------ Scope Variables -----
    $scope.user = {};
    $scope.terms = [];
    $scope.classes = [];
    $scope.loading = false;
    $scope.eventCount = 0;
    $scope.currentTerm = {};
    $scope.currentClass = {};

    //Calendar helpers

    $scope.getColor = function (n) {
        n = n * 2;
        n = 100 - n;
        var R = (255 * n) / 100
        var G = (255 * (100 - n)) / 100
        var B = 0
        return rgbToHex(R, G, B);
    }

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    $scope.exam = {
        term: "",
        name: "",
        class: "",
        duration: "",
        startTime: "",
        endTime: ""
    };

    $scope.getClassesForTerm = function () {
        $http.post("GetClassesByTerm", { termID: $scope.currentTerm.id, netID: $scope.user.netID }).then(function (result2) {
            if (result2 != null) {
                $scope.classes = result2.data.classes;
                if ($scope.classes.length > 0) {
                    $scope.currentClass = $scope.classes[0];
                }
            }
        });
    }

    $scope.generateCalendarEvents = function()
    {
        $scope.loading = true;
        $http.post("generateCalendarEvents", {duration: $scope.exam.duration, term: $scope.currentTerm.id, sbu: 1}).then(function (result) {
            if (result != null) {
                $scope.apptData = result.data.events;
                $scope.selectable = result.data.selectable;
                $scope.selectable.dow = [0, 1, 2, 3, 4, 5, 6];
                $scope.apptData.forEach(function (entry) {
                    entry.color = $scope.getColor(entry.title);
                    entry.rendering = 'background';
                });
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                    },
                    minTime: $scope.selectable.open,
                    maxTime: $scope.selectable.close,
                    forceEventDuration : true,
                    defaultTimedEventDuration : '00:30:00',
                    eventBackgroundColor: 'white',
                    defaultView: 'agendaWeek',
                    eventTextColor: 'black',
                    editable: true,
                    selectable: true,
                    eventLimit: 2,
                    selectHelper: true,
                    select: function (start, end) {
                        var eventData;
                        eventData = {
                            title: $scope.exam.name,
                            start: start,
                            end: end
                        };
                        if ($scope.eventCount == 0 && $scope.checkValidDates(new Date(start._d.toUTCString().substr(0, 25)), new Date(end._d.toUTCString().substr(0, 25)))) {
                            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                            $scope.exam.startTime = new Date(start._d.toUTCString().substr(0, 25));
                            $scope.exam.endTime = new Date(end._d.toUTCString().substr(0, 25));
                            $scope.eventCount++;
                        }$('#calendar').fullCalendar('unselect');
                        

                    },
                    eventDrop: function(event, delta, revertFunc) {
                        if ($scope.checkValidDates(new Date(event.start._d.toUTCString().substr(0, 25)), new Date(event.end._d.toUTCString().substr(0, 25)))) {
                            $scope.exam.startTime = new Date(event.start._d.toUTCString().substr(0, 25));
                            $scope.exam.endTime = new Date(event.end._d.toUTCString().substr(0, 25));
                        }
                        else
                            revertFunc();
                    },
                    eventResize: function (event, delta, revertFunc) {
                        if ($scope.checkValidDates(new Date(event.start._d.toUTCString().substr(0, 25)), new Date(event.end._d.toUTCString().substr(0, 25)))) {
                            $scope.exam.startTime = new Date(event.start._d.toUTCString().substr(0, 25));
                            $scope.exam.endTime = new Date(event.end._d.toUTCString().substr(0, 25));
                        }
                        else
                            revertFunc();
                    },
                    eventLimit: false, // allow "more" link when too many events
                    events: $scope.apptData,
                    loading: function (bool) {
                        $('#loading').toggle(bool);
                    }
                });
            }
            $scope.loading = false;
        });
    }

    $scope.checkValidDates = function (start, end) {
        var validStart = false;
        var validEnd = false;
        $scope.apptData.forEach(function (entry) {
            var eventStart = new Date(new Date(entry.start).toUTCString().substr(0, 25));
            if(eventStart.getTime() == start.getTime())
            {
                validStart = true;
            }
            if(eventStart.getTime() == end.getTime())
            {
                validEnd = true;
            }
        });
        return validStart && validEnd;
    }

    $scope.scheduleAnExam = function()
    {
        $http.post("scheduleAnExam", { name: $scope.exam.name, duration: $scope.exam.duration, startTime: $scope.exam.startTime, endTime: $scope.exam.endTime, termID: $scope.currentTerm.id, netID: $scope.user.netID, classID: $scope.currentClass.id }).then(function (result) {
            if(result != null)
            {
                $scope.successfulSchedule = (result.data == "True") ? true : false;
            }
        })
    }


    //------ Initial Calls ------
    // First get term info, and then you can load initial info
    $http.post("GetScheduleExamInfo", { sbu: 1 }).then(function (result) {
        if (result != null) {
            $scope.user = result.data.user;
            $scope.terms = result.data.terms;
            $scope.currentTerm = $scope.terms[0];
            $http.post("GetClassesByTerm", { termID: $scope.currentTerm.id, netID: $scope.user.netID}).then(function (result2) {
                if (result2 != null) {
                    $scope.classes = result2.data.classes;
                    if($scope.classes.length > 0)
                        $scope.currentClass = $scope.classes[0];
                }
            });
        }
    });
}

ScheduleExamController.$inject = ['$scope', '$http', '$timeout'];