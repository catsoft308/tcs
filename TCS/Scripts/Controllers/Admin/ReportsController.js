﻿var ReportsController = function ($scope, $http, $location, $timeout) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.selectedReport = 1;
    $scope.loading = false;
    $scope.reportData = [];
    $scope.terms = [];
    $scope.selectedTermR1 = {};
    $scope.selectedTermR2 = {};
    $scope.selectedTermR3 = {};
    $scope.selectedTermR41 = {};
    $scope.selectedTermR42 = {};


    //------ Scope Functions ------
    $scope.report1 = function () {
        $scope.selectedReport = 1;
        $scope.loading = true;

        $http.post("StudentAppointmentsInTermDaily", { termID: $scope.selectedTermR1.id }).then(function (result) {
            $scope.loading = false;

            if (result != null) {
                $scope.reportData = result.data;
            } else {
                $scope.reportData = [];
            }
        });
    }

    $scope.report2 = function () {
        $scope.selectedReport = 2;
        $scope.loading = true;

        $http.post("StudentAppointmentsInTermDaily", { termID: $scope.selectedTermR2.id }).then(function (result) {
            $scope.loading = false;

            if (result != null) {
                $scope.reportData = new Array();
                var dayCounter = 0;
                var weeklyCount = 0;
                for (var i = 0; i < result.data.length; i++) {
                    dayCounter++;
                    weeklyCount += result.data[i].appointments;
                    if (dayCounter == 7) {
                        $scope.reportData.push({ week: result.data[i-6].date, appointments: weeklyCount });
                        dayCounter = 0;
                        weeklyCount = 0;
                    }
                }
            } else {
                $scope.reportData = [];
            }
        });
    }

    $scope.report3 = function () {
        $scope.selectedReport = 3;
        $scope.loading = true;

        $http.post("CoursesInTerm", { termID: $scope.selectedTermR3.id }).then(function (result) {
            $scope.loading = false;

            if (result != null) {
                $scope.reportData = result.data;
            } else {
                $scope.reportData = [];
            }
        });
    }

    $scope.report4 = function () {
        $scope.selectedReport = 4;
        $scope.loading = true;

        $http.post("StudentAppointmentCountOverTerms", { startTermID: $scope.selectedTermR41.id, endTermID: $scope.selectedTermR42.id }).then(function (result) {
            $scope.loading = false;

            if (result != null) {
                $scope.reportData = result.data;
            } else {
                $scope.reportData = [];
            }
        });
    }


    //------ Initial Calls ------
    // First get term info, and then you can load initial info
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result != null) {
            $scope.user = result.data.user;
            $scope.terms = result.data.terms;
            $scope.selectedTermR1 = $scope.terms[0];
            $scope.selectedTermR2 = $scope.terms[0];
            $scope.selectedTermR3 = $scope.terms[0];
            $scope.selectedTermR41 = $scope.terms[0];
            $scope.selectedTermR42 = $scope.terms[0];

            $scope.report1();
        } else {

        }
    });
}

ReportsController.$inject = ['$scope', '$http', '$location', '$timeout'];