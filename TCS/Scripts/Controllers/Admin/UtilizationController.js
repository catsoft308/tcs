﻿var UtilizationController = function ($scope, $http, $location, $timeout) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.terms = [];
    $scope.selectedTerm = {};
    $scope.loading = false;
    $scope.utilization = {};
    $scope.startDate = {};
    $scope.endDate = {};

    /* Datepicker stuff */
    $scope.toggleOpenStart = function () {
        var x = true;
        $timeout(function () {
            $scope.openedStart = !$scope.openedStart;
        });
    }

    $scope.toggleOpenEnd = function () {
        var x = true;
        $timeout(function () {
            $scope.openedEnd = !$scope.openedEnd;
        });
    }

    // Only enable dates which the test is being given
    $scope.disabled = function (date, mode) {
        date.setHours("00");
        var min = $scope.minDate;
        min.setHours("00");
        min.setMinutes("00");
        min.setSeconds("00");
        var t = (mode === 'day' && (date > $scope.maxDate || date < min));
        return t;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    };

    $scope.configPickers = function () {
        $scope.minDate = new Date($scope.selectedTerm.startDate);
        $scope.maxDate = new Date($scope.selectedTerm.endDate);
        $scope.startDate = new Date($scope.selectedTerm.startDate);
        $scope.endDate = new Date($scope.selectedTerm.endDate);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.showButtonBar = false;

        $scope.format = 'dd-MMMM-yyyy';

        $scope.opened = false;
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 2);
        $scope.events =
          [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
          ];
    }


    //------ Scope Functions ------
    $scope.getUtilization = function () {
        if ($scope.startDate <= $scope.endDate) {
            $scope.utilization = {};
            $scope.loading = true;

            $http.post("GetUtilization", { termID: $scope.selectedTerm.id, startDate: $scope.startDate.toISOString(), endDate: $scope.endDate.toISOString() }).then(function (result) {
                $scope.loading = false;

                if (result != null) {
                    $scope.utilization = result.data;
                } else {

                }
            });
        }
        else {
            alert("Please select a valid date range.");
        }
    }

    $scope.isEmptyObject = function (obj) {
        for (var prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                return false;
            }
        }
        return true;
    }


    //------ Initial Calls ------
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result != null) {
            $scope.user = result.data.user;
            $scope.terms = result.data.terms;
            $scope.selectedTerm = $scope.terms[0];

            $scope.configPickers();
        } else {

        }
    });
}

UtilizationController.$inject = ['$scope', '$http', '$location', '$timeout'];