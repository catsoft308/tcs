﻿var ImportDataController = function ($scope, $http, $location, tcs) {

    //------ Scope Variables ------
    $scope.user = { firstName: 'Admin' };   // TODO
    // all data from our form
    $scope.formData = {
        importType: "users",
        terms: [],
        selectedTerm: null
    };

    $scope.lastSelectedImportType = $scope.formData.importType;

    //------ Scope Functions ------
    $scope.submit = function () {
        if (!$scope.checkFileType()) {
            // TODO alert error
            return;
        }

        var doTheImport = function (path) {
            $http.post(path, { })
                .then(function (result) {
                    if (result.data === true) {
                        // TODO show success
                        console.log("Import Success!");
                    } else {
                        // TODO show failure
                        console.log("Import failed");
                    }
                });
        }

        switch ($scope.formData.importType) {
            case "students":
                doTheImport("ImportStudents");
                break;
            case "instructors":
                doTheImport("ImportInstructors");
                break;
            case "classes":
                doTheImport("ImportClasses");
                break;
            case "roster":
                doTheImport("ImportRoster");
                break;
            default:
                break;
        }
    }

    // make sure it's a csv
    $scope.checkFileType = function () {
        var path = $("input:file").val();
        // no file
        if (path.length === 0) return false;

        var dotIndex = path.lastIndexOf('.');
        var ext = path.substr(dotIndex);

        // file isn't csv
        if (ext !== ".csv")
            return false;

        return true;
    }

    //------ Initial Calls ------
    $http.post("GetTerms", {}).then(function (result) {
        if (result != null) {
            $scope.formData.terms = result.data;
            $scope.formData.selectedTerm = $scope.formData.terms[0];
        } else {
            // TODO feedback letting user know this failed
        }
    });

    tcs.prepareFileInput();

    // -- Watches --
    //$scope.$watch('formData.importType', function (value) {
    //    if (value === null) {
    //        $scope.formData.importType = $scope.lastSelectedImportType;
    //    } else {
    //        $scope.lastSelectedImportType = $scope.formData.importType;
    //    }
    //});
}

ImportDataController.$inject = ['$scope', '$http', '$location', 'tcs'];