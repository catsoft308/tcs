﻿var AdminMakeAppointmentController = function ($scope, $http, $timeout) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.terms = [];
    $scope.complete = false;
    $scope.exam = {};
    $scope.appt = {};
    $scope.appt.setAsideSeat = false;
    $scope.loading = false;

    /* Timepicker stuff */
    $scope.generateTime = function () {
        var opens = ($scope.appt.time.getDay() == 6 || $scope.appt.time.getDay() == 0) ? $scope.wkndOpen : $scope.wkdyOpen;
        var closes = ($scope.appt.time.getDay() == 6 || $scope.appt.time.getDay() == 0) ? $scope.wkndClose : $scope.wkdyClose;

        //subtract exam duration from closes time; can't take test after center closes
        var hms = closes.split(":");
        var temp = new Date(2000, 1, 1, hms[0], hms[1], hms[2]);
        var subbed = new Date(temp - $scope.exam.duration * 60 * 1000);
        closes = subbed.getHours() + ':' + subbed.getMinutes() + ":00";

        if ($scope.exam.start.getDate() == $scope.appt.time.getDate()) {
            $scope.minTime = $scope.exam.start.toTimeString().substring(0, 8);
            $scope.maxTime = closes;
        }
        else if ($scope.exam.end.getDate() == $scope.appt.time.getDate()) {
            $scope.maxTime = $scope.exam.end.toTimeString().substring(0, 8);
            $scope.minTime = opens;
        }
        else {
            $scope.maxTime = closes;
            $scope.minTime = opens;
        }
        if (new Date().getDate() == $scope.appt.time.getDate()) {
            hms = new Date().toTimeString().substring(0, 8).split(":");
            var hour = parseInt(hms[0]);
            var minute = parseInt(hms[1]);
            if (minute > 30) {
                hour++;
                minute = "00";
            }
            else {
                minute = 30;
            }

            $scope.minTime = hour + ":" + minute + ":00";

            $scope.appt.time.setHours(hour);
            $scope.appt.time.setMinutes(minute);
            $scope.appt.time.setSeconds(0);
        }
        else {
            hms = $scope.minTime.split(":");
            $scope.appt.time.setHours(hms[0]);
            $scope.appt.time.setMinutes(hms[1]);
            $scope.appt.time.setSeconds(0);

        }
        $('#timePicker').timepicker('option', 'minTime', $scope.minTime)
        $('#timePicker').timepicker('option', 'maxTime', $scope.maxTime)
    }

    $scope.timePickerOptions = {
        step: 30,
        timeFormat: 'g:i A'
    };


    /* Datepicker stuff */
    $scope.toggleOpen = function () {
        var x = true;
        $timeout(function () {
            $scope.opened = !$scope.opened;
        });
    }

    // Only enable dates which the test is being given
    $scope.disabled = function (date, mode) {
        date.setHours("00");
        var min = $scope.minDate;
        min.setHours("00");
        min.setMinutes("00");
        min.setSeconds("00");
        var t = (mode === 'day' && (date > $scope.maxDate || date < min));
        return t;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    };


    //------ Scope Functions ------
    $scope.requestAppointment = function () {
        $http.post("RequestAppointmentForStudent", {
            startTime: $scope.appt.time.toISOString(),
            setAsideSeat: $scope.appt.setAsideSeat
        }).then(function (result) {
            $scope.appt.approved = result.data;
            $scope.complete = true;
        });
    }


    //------ Initial Calls ------
    $http.post("GetBasicInfo", {}).then(function (result) {
        $scope.loading = true;
        if (result != null) {
            $scope.terms = result.data.terms;
            $scope.user = result.data.user;
            $http.post("loadMkAppData").then(function (result2) {
                if (result2 != null) {
                    $scope.exam = result2.data.ex;
                    $scope.exam.start = new Date($scope.exam.start);
                    $scope.exam.end = new Date($scope.exam.end);
                    $scope.wkndOpen = result2.data.hours.wkndOpen;
                    $scope.wkndClose = result2.data.hours.wkndClose;
                    $scope.wkdyOpen = result2.data.hours.wkdyOpen;
                    $scope.wkdyClose = result2.data.hours.wkdyClose;
                    $scope.configPickers();
                    $scope.generateTime();
                } else {
                    console.log("Error loading data.");
                }
            });
        } else {
            console.log("Error getting info.");
        }
        $scope.loading = false;
    });

    $scope.configPickers = function () {
        $scope.appt.time = (new Date($scope.exam.start) > new Date()) ? new Date($scope.exam.start) : new Date();
        $scope.minDate = (new Date($scope.exam.start) > new Date()) ? new Date($scope.exam.start) : new Date();
        $scope.maxDate = new Date($scope.exam.end);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.showButtonBar = false;

        $scope.format = 'dd-MMMM-yyyy';

        $scope.opened = false;
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 2);
        $scope.events =
          [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
          ];
    }
}