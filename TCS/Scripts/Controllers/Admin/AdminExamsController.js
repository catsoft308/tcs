﻿var AdminExamsController = function ($scope, $http) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.terms = [];
    $scope.selectedTerm = {};
    $scope.approvedExams = [];
    $scope.pendingExams = [];
    $scope.currentExam = {};        // The current exam you are canceling or looking at
    $scope.currentAppointment = {}; //The current appointment you are editing
    $scope.studentAppointments = [];
    $scope.utilization = [];    // Utilization info for exam when deciding to approve an exam
    $scope.loading = false;
    $scope.modalLoading = false;


    //------ Scope Functions ------
    $scope.loadInfo = function () {
        $scope.loading = true;
        
        $http.post("GetExams", { termID: $scope.selectedTerm.id }).then(function (result) {
            $scope.loading = false;

            if (result != null) {
                $scope.approvedExams = result.data.approvedExams;
                $scope.pendingExams = result.data.pendingExams;
            } else {
                // TODO error
            }
        });
    }

    // Set the current exam to be used in other functions
    $scope.setCurrentExam = function (exam) {
        $scope.currentExam = exam;
    }

    // Set appointment to be edited
    $scope.setCurrentAppointment = function (appointment) {
        $scope.currentAppointment.id = appointment.id;
        $scope.currentAppointment.studentID = appointment.studentID;
        $scope.currentAppointment.seatNumber = appointment.seatNumber;
        // date sent in format "/Date(timeval)/"
        $scope.currentAppointment.date = eval('new ' + appointment.time.slice(1, -1));
        $scope.currentAppointment.time = $scope.currentAppointment.date;
    }

    $scope.cancelExam = function () {
        $http.post("CancelExam", { examID: $scope.currentExam.id }).then(function (result) {
            alert(result.data);
            $scope.loadInfo(); //refresh
        });
    }

    $scope.getUtilization = function (exam) {
        $scope.modalLoading = true;
        $scope.setCurrentExam(exam);

        $http.post("GetUtilizationHypothetical", JSON.stringify({ termID: exam.termID, startDate: exam.startTimeISOString, endDate: exam.endTimeISOString, examID: exam.id }))
            .then(function (result) {
                $scope.modalLoading = false;
                $scope.utilization = result.data;
            }
        );
    }

    $scope.seeExamInfo = function (exam) {
        $scope.currentExam = exam;
        $scope.modalLoading = true;

        $http.post("GetExamInfo", { examID: exam.id }).then(function (result) {
            $scope.modalLoading = false;

            if (result != null) {
                $scope.studentAppointments = result.data;
            } else {
                // TODO error
            }
        });
    }

    $scope.cancelAppointment = function () {
        $scope.modalLoading = true;

        $http.post("CancelAppointment", {appointmentID: $scope.currentAppointment.id}).then(function (result) {
            $scope.modalLoading = false;

            alert(result.data);
            $scope.seeExamInfo($scope.currentExam); //refresh
        });
    }

    $scope.editAppointment = function () {
        $scope.modalLoading = true;

        $http.post("EditAppointment", {
            appointmentID: $scope.currentAppointment.id,
            seatNumber: $scope.currentAppointment.seatNumber,
            date: $scope.currentAppointment.date,
            time: $scope.currentAppointment.time
        }).then(function (result) {
            $scope.modalLoading = false;

            alert(result.data);
            $scope.seeExamInfo($scope.currentExam); //refresh
        });
    }

    $scope.checkInStudent = function (studentAppointment) {
        $http.post("CheckInStudent", { appointmentID: studentAppointment.id }).then(function (result) {
            alert(result.data);
            $scope.seeExamInfo($scope.currentExam); //refresh
        });
    }

    $scope.approveExamRequest = function () {
        $http.post("ApproveRequest", { examID: $scope.currentExam.id }).then(function (result) {
            alert(result.data);
            $scope.loadInfo(); //refresh
        });
    }

    $scope.denyExamRequest = function () {
        $http.post("DenyRequest", { examID: $scope.currentExam.id }).then(function (result) {
            alert(result.data);
            $scope.loadInfo(); //refresh
        });
    }


    //------ Initial Calls ------
    // First get term info, and then you can load initial info
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result != null) {
            $scope.terms = result.data.terms;
            $scope.user = result.data.user;
            $scope.selectedTerm = $scope.terms[0];

            $scope.loadInfo();
        } else {
            // TODO error
        }
    });
}

AdminExamsController.$inject = ['$scope', '$http'];
