﻿var AdminHomeController = function ($scope, $http, $location, $timeout) {

    //------ Scope Variables ------
    $scope.user = {};

    //------ Initial Calls ------
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result != null) {
            $scope.user = result.data.user;
        } else {
            // User error
        }
    });
}

AdminHomeController.$inject = ['$scope', '$http', '$location', '$timeout'];