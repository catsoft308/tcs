﻿var EditCenterInfoController = function ($scope, $http, $location) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.terms = [];
    $scope.selectedTerm = {};

    // Scope var for each editable attribute
    $scope.numSeats = '';
    $scope.numSetAsideSeats = '';
    $scope.centerHours = {}; // Fields for openTime and closeTime for weekday and weekend
    $scope.centerHoursTerm = {};
    $scope.openDates = {}; // Fields for startDate and endDate
    $scope.openDatesTerm = {};
    $scope.nonSBUDates = {}; // Fields for startDate and endDate
    $scope.nonSBUDatesTerm = {};
    $scope.gapTime = '';
    $scope.reminderInterval = '';


    //------ Scope Functions ------
    $scope.submitFunction = function () { }; // The function that references the actual call

    // The function that makes the back end call to the given method
    var submitChange = function (methodName, data) {
        $http.post(methodName, data).then(function (result) {
            if (result != null) {
                // Show response modal with returned message.
                alert(result.data);
            } else {

            }
        });
    }

    $scope.changeNumSeats = function () {
        var data = {
            numSeats: $scope.numSeats
        };
        submitChange("EditNumSeats", data);
    }

    $scope.changeNumSetAsideSeats = function () {
        var data = {
            numSetAsideSeats: $scope.numSetAsideSeats
        };
        submitChange("EditNumSetAsideSeats", data);
    }

    $scope.changeCenterHours = function () {
        var data = {
            termID: $scope.centerHoursTerm.id,
            weekdayOpen: $scope.centerHours.openTimeWeekday.toISOString(),
            weekdayClose: $scope.centerHours.closeTimeWeekday.toISOString(),
            weekendOpen: $scope.centerHours.openTimeWeekend.toISOString(),
            weekendClose: $scope.centerHours.closeTimeWeekend.toISOString()
        };
        submitChange("EditCenterHours", data);
    }

    $scope.changeOpenDates = function () {
        var data = {
            termID: $scope.openDatesTerm.id,
            startDate: $scope.openDates.startDate.toISOString(),
            endDate: $scope.openDates.endDate.toISOString()
        };
        submitChange("EditOpenDates", data);
    }

    $scope.changeNonSBUDates = function () {
        var data = {
            termID: $scope.nonSBUDatesTerm.id,
            startDate: $scope.nonSBUDates.startDate,
            endDate: $scope.nonSBUDates.endDate
        };
        submitChange("EditNonSBUDates", data);
    }

    $scope.changeGapTime = function () {
        var data = {
            gapTime: $scope.gapTime
        };
        submitChange("EditGapTime", data);
    }

    $scope.changeReminderInterval = function () {
        var data = {
            reminderInterval: $scope.reminderInterval
        };
        submitChange("EditReminderInterval", data);
    }

    // Get times for centerHoursTerm
    $scope.centerHoursInfo = function () {
        $http.post("GetHours", { termID: $scope.centerHoursTerm.id }).then(function (result) {
            if (result != null) {
                // date sent in format "/Date(timeval)/"
                $scope.centerHours.openTimeWeekday = eval('new ' + result.data.openTimeWeekdayString.slice(1, -1));
                $scope.centerHours.closeTimeWeekday = eval('new ' + result.data.closeTimeWeekdayString.slice(1, -1));
                $scope.centerHours.openTimeWeekend = eval('new ' + result.data.openTimeWeekendString.slice(1, -1));
                $scope.centerHours.closeTimeWeekend = eval('new ' + result.data.closeTimeWeekendString.slice(1, -1));
            }
            else {

            }
        });
    }

    // Get dates for openDatesTerm
    $scope.openDatesInfo = function () {
        $http.post("GetOpenDates", { termID: $scope.openDatesTerm.id }).then(function (result) {
            if (result != null) {
                // date sent in format "/Date(timeval)/"
                $scope.openDates.startDate = eval('new ' + result.data.startDateString.slice(1, -1));
                $scope.openDates.endDate = eval('new ' + result.data.endDateString.slice(1, -1));
            }
            else {

            }
        });
    }

    // Get dates for nonSBUDatesTerm
    $scope.nonSBUDatesInfo = function () {
        $http.post("GetNonSBUDates", { termID: $scope.nonSBUDatesTerm.id }).then(function (result) {
            if (result != null) {
                // date sent in format "/Date(timeval)/"
                $scope.nonSBUDates.startDate = eval('new ' + result.data.startDateString.slice(1, -1));
                $scope.nonSBUDates.endDate = eval('new ' + result.data.endDateString.slice(1, -1));
            }
            else {

            }
        });
    }


    //------ Initial Calls ------
    // First get term info, and then you can load initial info
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result != null) {
            $scope.terms = result.data.terms;
            $scope.user = result.data.user;
            $scope.selectedTerm = $scope.terms[0];

            $http.post("GetCurrentCenterInfo", {termID: $scope.selectedTerm.id}).then(function (result2) {
                if (result2 != null) {
                    $scope.numSeats = result2.data.numSeats;
                    $scope.numSetAsideSeats = result2.data.numSetAsideSeats;
                    $scope.centerHours = result2.data.centerHours.Data;
                    // date sent in format "/Date(timeval)/"
                    $scope.centerHours.openTimeWeekday = eval('new ' + $scope.centerHours.openTimeWeekdayString.slice(1,-1));
                    $scope.centerHours.closeTimeWeekday = eval('new ' + $scope.centerHours.closeTimeWeekdayString.slice(1,-1));
                    $scope.centerHours.openTimeWeekend = eval('new ' + $scope.centerHours.openTimeWeekendString.slice(1,-1));
                    $scope.centerHours.closeTimeWeekend = eval('new ' + $scope.centerHours.closeTimeWeekendString.slice(1,-1));
                    $scope.centerHoursTerm = $scope.selectedTerm;
                    $scope.openDates = result2.data.openDates.Data;
                    $scope.openDates.startDate = eval('new ' + $scope.openDates.startDateString.slice(1,-1));
                    $scope.openDates.endDate = eval('new ' + $scope.openDates.endDateString.slice(1, -1));
                    $scope.openDatesTerm = $scope.selectedTerm;
                    $scope.nonSBUDates = result2.data.nonSBUDates.Data;
                    $scope.nonSBUDates.startDate = eval('new ' + $scope.nonSBUDates.startDateString.slice(1, -1));
                    $scope.nonSBUDates.endDate = eval('new ' + $scope.nonSBUDates.endDateString.slice(1, -1));
                    $scope.nonSBUDatesTerm = $scope.selectedTerm;
                    $scope.gapTime = result2.data.gapTime;
                    $scope.reminderInterval = result2.data.reminderInterval;
                } else {
                    // System data error
                }
            });
        } else {
            // User error
        }
    });
}

EditCenterInfoController.$inject = ['$scope', '$http', '$location'];