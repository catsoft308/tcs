﻿var ScheduleExamForStudentController = function ($scope, $http, $location, $timeout) {

    //------ Scope Variables ------
    $scope.user = {};
    $scope.loading = false;
    $scope.studentID = '';
    $scope.matchedID = '';
    $scope.studentExams = [];


    //------ Scope Functions ------
    $scope.getStudent = function () {
        $scope.loading = true;
        $scope.matchedID = $scope.studentID;

        $http.post("GetStudentExams", { studentID: $scope.matchedID }).then(function (result) {
            $scope.loading = false;

            if (result != null) {
                $scope.studentExams = result.data;
                if (result.data.length < 1)
                    alert("No upcoming exams for " + $scope.matchedID);
            } else {

            }
        });
    }

    $scope.makeAppointment = function (exam) {
        if ($scope.studentExams.length > 0)
            window.location = 'MakeAppointment?examID=' + exam.id + '&studentID=' + $scope.matchedID;
    }


    //------ Initial Calls ------
    $http.post("GetBasicInfo", {}).then(function (result) {
        if (result != null) {
            $scope.terms = result.data.terms;
            $scope.user = result.data.user;
            $scope.selectedTerm = $scope.terms[0];
        } else {

        }
    });
}

ScheduleExamForStudentController.$inject = ['$scope', '$http', '$location', '$timeout'];