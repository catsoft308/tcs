﻿var LoginController = function ($scope, $http) {
    $scope.form = {
        username: '',
        password: ''
    };
    $scope.respModal = false;
    $scope.role = []

    $scope.login = function () {
        $http.post("Home/userLogin", $scope.form).then(function(result)
        {
            $scope.respModal = true;
            var loc = "";
            var role = result.data.split("");
            for (var i = 0; i < role.length; i++) { role[i] = +role[i]; }
            if (role[0] + role[1] + role[2] == 1)
            {
                if (role[0] == 1)
                    loc = "Admin";
                if (role[1] == 1)
                    loc = "Students";
                if (role[2] == 1)
                    loc = "Professors";
                $scope.loginResp = "Logging in...";
            }
            else if (role[0] + role[1] + role[2] == 0)
            {
                $scope.loginResp = "Invalid NetID/Password.";
            }
        else
            {
              $scope.loginResp = "Please choose a portal to enter.";
            }
            $scope.role = role;

            if(loc != "")
                window.location = "/" + loc + "/Home";
        });
    }
}

LoginController.$inject = ['$scope', '$http'];