﻿using System;

namespace TCS.App_Data
{
    /// <summary>
    /// Enums to be used within the TCS project
    /// </summary>
    public class Enums
    {
        /// <summary>
        /// Used to denote the level of access a user has to the site. Treated as bit flags.
        /// </summary>
        [Flags]
        public enum Role
        {
            NoRole = 0,
            Instructor = 1,
            Student = 2,
            StudIns = 3,
            Admin = 4,
            AdIns = 5,
            AdStud = 6,
            All = 7
        }
    }
}