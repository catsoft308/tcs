﻿using System;
using System.Linq;

namespace TCS.App_Data
{
    public class Utils
    {
        public static string InitialPassword(string netId)
        {
            return netId + "2015";
        }


        #region Role Checking
        public static bool IsRole(sbyte role, Enums.Role desiredRole)
        {
            return (role & (sbyte)desiredRole) == (sbyte)desiredRole;
        }

        public static bool IsStudent(sbyte role)
        {
            return IsRole(role, Enums.Role.Student) || IsRole(role, Enums.Role.AdStud) ||
                IsRole(role, Enums.Role.StudIns) || IsRole(role, Enums.Role.All);
        }

        public static bool IsInstructor(sbyte role)
        {
            return IsRole(role, Enums.Role.Instructor) || IsRole(role, Enums.Role.AdIns) ||
                IsRole(role, Enums.Role.StudIns) || IsRole(role, Enums.Role.All);
        }

        public static bool IsAdmin(sbyte role)
        {
            return IsRole(role, Enums.Role.Admin) || IsRole(role, Enums.Role.AdStud) ||
                IsRole(role, Enums.Role.AdIns) || IsRole(role, Enums.Role.All);
        }

        public static String getRoleString(sbyte role)
        {
            string roleString = "";
            string admin = "0", prof = "0", stud = "0";
            if (IsAdmin(role))
                admin = "1";
            if (IsInstructor(role))
                prof = "1";
            if (IsStudent(role))
                stud = "1";
            roleString = admin + stud + prof;

            return roleString;
        }

        public static bool AnyNullOrEmpty(string[] strings)
        {
            return strings.Any(String.IsNullOrEmpty);
        }

        #endregion
    }

    public static class ExtensionMethods
    {
        /// <summary>
        /// Returns a timestamp in given format or yyyyMMddHHmmssffff by default
        /// </summary>
        public static string Timestamp(this DateTime value, string format = "yyyyMMddHHmmssffff")
        {
            return value.ToString(format);
        }

        /// <summary>
        /// Returns whether this datetime is within mon-fri (work week)
        /// </summary>
        public static bool IsWeekday(this DateTime d)
        {
            var dayOfWeek = d.DayOfWeek;
            return (dayOfWeek >= DayOfWeek.Monday && dayOfWeek <= DayOfWeek.Friday);
        }

        /// <summary>
        /// (Deprecated) Returns whether this DateTime falls between DateTimes a and b.
        /// </summary>
        public static bool IsBetweenDates(this DateTime d, DateTime? a, DateTime? b)
        {
            if (!a.HasValue || !b.HasValue) return false;

            // Switch them around if B is actually before A
            if (b.Value.Ticks < a.Value.Ticks)
            {
                var temp = b;
                b = a;
                a = temp;
            }

            return d.Ticks > a.Value.Ticks && d.Ticks < b.Value.Ticks;
        }
    }
}