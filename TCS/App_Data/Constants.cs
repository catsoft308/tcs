﻿namespace TCS.App_Data
{
    /// <summary>
    /// Constants to be used within the TCS project
    /// </summary>
    public class Constants
    {
        public const string DEFAULT_PW = "password";
        public const Enums.Role DEFAULT_ROLE = Enums.Role.Student;

        public class ImportMethods
        {
            public const string STUDENTS = "students";
            public const string INSTRUCTORS = "instructors";
            public const string CLASSES = "classes";
            public const string ROSTER = "roster";
        }

        public class SessionStrings
        {
            public const string USER_ID = "userID";
            public const string ROLES = "roles";
            public const string CURRENT_EXAM = "currentExam";
            public const string STUDENT_ID = "studentID";
        }
    }
}
